### How To Use it

To Run project simply using maven `mvn clean verify`

To Run pararely , Simply change the properties from `junit-platform.properties`

>`cucumber.execution.parallel.config.fixed.parallelism=2`

2 Mean you will run by 2 paralel or you can override the properties from maven command 

>`mvn clean verify -D"cucumber.execution.parallel.config.fixed.parallelism"=2`
### Configuration
To Change the tags , You can change the properties from junit-platform.properties

>`cucumber.filter.tags=@Regression`

with @Regression is your tags that you want to run , more doccumentation about filtering by tags , you can find it in this link [Cucumber Docs](https://cucumber.io/docs/cucumber/api/#tags)

### Requirement 

- Java 8 
- Browsers ( Chrome or Firefox )
- Intellij
- Git

### ToDo
- Refaktor Driver
- Coba Test