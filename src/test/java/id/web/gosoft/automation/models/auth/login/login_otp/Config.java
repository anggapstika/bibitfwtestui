
package id.web.gosoft.automation.models.auth.login.login_otp;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@SuppressWarnings("unused")
public class Config {

  @JsonProperty("bonus_product")
  private String bonusProduct;
  @JsonProperty("payment_channel")
  private List<String> paymentChannel;
  @JsonProperty("robo_min_topup")
  private Long roboMinTopup;

}
