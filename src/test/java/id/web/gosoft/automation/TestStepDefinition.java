package id.web.gosoft.automation;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestStepDefinition {
    private WebDriver webDriver;
    @Given("user open browser")
    public void userOpenBrowser() {
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.get("https://www.google.com/");
    }

    @When("user waiting for {int} seconds")
    public void userWaitingForSeconds(int wait) throws InterruptedException {
        Thread.sleep(wait*1000);
    }

    @Then("user stop browser")
    public void userStopBrowser() {
        webDriver.quit();
    }

    @Given("user open browser {string}")
    public void userOpenBrowser(String arg0) {
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.get(arg0);
    }
}
