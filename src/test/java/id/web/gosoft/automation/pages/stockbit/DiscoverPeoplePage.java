package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class DiscoverPeoplePage extends BasePageObject {

    public void scrollToTrendingSection(){
        WebElement element = find(By.cssSelector(".stream-box"));
        int y = element.getLocation().getY();
        scrollByPixels(0, y + 100);
    }


    public void clickAddDiscover(){
        clickOn(By.cssSelector(".ant-card-see-all"));
    }


    public String getFollowText(){
        return getText(By.xpath("//div[@class='discover-list-wrap']//child::div[1]/div[1]/div[3]"));
    }

    public String getFollowersText(){
        return getText(By.xpath("//div[@class='follower-list search']//child::div[1]/div[3]"));
    }

    public String getFollowersFollowingText(){
        return getText(By.xpath("//div[@class='follower-list search']//child::div[1]/div[3]"));
    }

    public void clickFollow(){
    clickOn(By.xpath("//div[@class='discover-list-wrap']//child::div[1]/div[1]/div[3]"));
    }

    public void clickOneOfFollowers(){
        clickOn(By.xpath("//div[@class='follower-list search']//child::div[1]/div[3]"));
    }

    public String getFollowersFollowText(){
        return getText(By.xpath("//div[@class='follower-list search']//child::div[1]/div[3]"));
    }


    public String getFollowingText(){
        return getText(By.xpath("//div[@class='discover-list-wrap']//child::div[1]/div[1]/div[3]"));
    }

    public void clickFollowing() {
       clickOn(By.xpath("//div[@class='discover-list-wrap']//child::div[1]/div[1]/div[3]"));
    }


    public void inputSearchDiscover(String discover){
//        typeOn(By.cssSelector(".discover-search"), discover);
        typeOn(By.cssSelector(".ant-input"), discover);

        waitABit(3);
//        find(By.cssSelector(".discover-search")).sendKeys(Keys.ENTER);
        find(By.cssSelector(".ant-input")).sendKeys(Keys.ENTER);
    }

    public String getSearchResult(){
        return getText(By.xpath("//div[@class='discover-list']//child::div[1]/div[2]/div[1]/a"));
    }

    public void scrollUntilSuggestion(){
        do {
            scrollToBottom();
        } while (!isVisible(By.xpath("//div[@class='stream-box sg-wrap-box']")));

        waitABit(3);
        clickOn(By.cssSelector(".trigger-click-suggested"));
    }

    public boolean isDiscoverPeopleMenu(){
        return isPresent(By.cssSelector(".discover-people-wrap"));
    }

    public boolean isFollowerListPage(){
        return isPresent(By.cssSelector(".follower-in"));
    }

    public void clickFollowers(){
        clickOn(By.xpath("//span[contains(text(),'Followers')]"));
    }

    public void inputSearchFollowers(String followers){
            typeOn(By.cssSelector(".input-search-followers-following>input"), followers);
    }

    public String getNotificationSearch(){
        return getText(By.cssSelector(".main-profile-searched-notfound"));
    }

    public void userClickCloseSearch(){
        clickOn(By.cssSelector(".icon-close-search"));
    }

    public void clickFirstResultFollow(){
        clickOn(By.xpath("//div[@class='follower-list search']//child::div[1]/div[2]"));
    }

    public String getUserSearchFollowerResult(){
        return getText(By.xpath("//div[@class='follower-list search']//child::div[1]/div[2]/a"));
    }

}
