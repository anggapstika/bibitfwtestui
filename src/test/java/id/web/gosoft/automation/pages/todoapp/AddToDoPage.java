package id.web.gosoft.automation.pages.todoapp;

import id.web.gosoft.automation.webdriver.AndroidDriverInstance;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;

public class AddToDoPage {

  public void inputTitle(String title) {
    AndroidElement inputTitle = (AndroidElement) AndroidDriverInstance.webDriver
        .findElement(MobileBy.id("add_task_title"));
    inputTitle.sendKeys(title);
  }

  public void inputDescription(String description) {
    AndroidElement inputDescription = (AndroidElement) AndroidDriverInstance.webDriver
        .findElement(MobileBy.id("add_task_description"));
    inputDescription.sendKeys(description);
  }

  public void clickSubmit() {
    AndroidElement buttonSubmit = (AndroidElement) AndroidDriverInstance.webDriver
        .findElement(MobileBy.id("fab_edit_task_done"));
    buttonSubmit.click();
  }

}
