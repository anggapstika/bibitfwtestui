package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class MainPage extends BasePageObject {
    public boolean isOnPage() {
        WebDriverWait wait = new WebDriverWait(getDriver(), 15);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main-wrapper")))
                .isDisplayed();
    }

    public void clickMenu(By by) {
        if (!isVisible(by)) clickOn(By.className("navmore"));
        clickOn(by);
    }

    public void clickStreamMenu() {
//        By by = By.id("homeLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Stream']"));
    }

    public void clickProfileMenu() {
//        By by = By.id("profileLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Profile']"));
    }


    public void clickWatchlistMenu() {
//        By by = By.id("watchlistLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Watchlist']"));
    }

    public void clickChatMenu() {
//        By by = By.id("chatLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Chat']"));
    }

    public void clickChartbitMenu() {
        By by = By.id("chartbitLink");
        clickMenu(by);
    }

    public void clickOrderbookMenu() {
//        By by = By.id("orderbookLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Orderbook']"));
    }

    public void clickScreenerMenu() {
//        By by = By.id("screenerLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Screener']"));
    }

    public void clickValuationMenu() {
//        By by = By.id("valuationLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Valuation']"));
    }

    public void clickFinancialsMenu() {
//        By by = By.id("financialsLink");
//        clickMenu(by);
        clickOn(By.xpath("//i[text()='Financials']"));
    }

    public void clickFundachartMenu() {
        By by = By.id("fundachartLink");
        clickMenu(by);
    }

    public void clickSeasonalityMenu() {
        By by = By.id("seasonalityLink");
        clickMenu(by);
    }

    public void clickSectorMenu() {
        By by = By.id("sectorLink");
        clickMenu(by);
    }

    public void clickCalendarMenu() {
        By by = By.id("calendarLink");
        clickMenu(by);
    }

    public void clickEarningsMenu() {
        By by = By.id("earningsLink");
        clickMenu(by);
    }

    public void clickGlossaryMenu() {
        By by = By.id("glossaryLink");
        clickMenu(by);
    }

    public void clickEbookMenu() {
        By by = By.id("ebookLink");
        clickMenu(by);
    }

    public String getAlertMessage() {
//        return getText(By.cssSelector(".sysmsg-content"));
        return getText(By.xpath("//div[@class='sysmsg-content']/div"));
//        return getText(By.xpath("//div[@class='sysmsg']"));
    }

    public void toLatestTab() {
        ArrayList<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(tabs.size() - 1));
    }

    public void clickHeaderMenu() {
        clickOn(By.cssSelector(".headermenu.user a"));
    }

    public void logout() {
        clickOn(By.cssSelector(".headermenu.user a"));
        clickOn(By.cssSelector("a[href='#/logout']"));
    }
}
