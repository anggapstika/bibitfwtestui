package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;


public class AcademyPage extends BasePageObject {

    public void clickAcademy(){
        clickOn(By.cssSelector(".icon-menu_academy"));
    }

    public void clickModule1() {
        clickOn(By.cssSelector(".sc-eJocfa"));
    }

    public void clickChapter1() {
        do {
            scrollToBottom();
        } while (!isVisible(By.xpath(".sc-cBoqAE")));

        waitABit(3);
        clickOn(By.cssSelector(".sc-ezzafa"));

    }

    public void clicklesson1(){
        clickOn(By.cssSelector(".sc-dkQUaI"));
    }

    public void userClickPlayVideo(){
        clickOn(By.cssSelector(".react-player__play-icon"));
        waitABit(3);
        clickOn(By.cssSelector(".ytp-play-button"));
    }

    public boolean lessonIsDisplayed(){
        return isDisplayed(By.cssSelector(".sticky-outer-wrapper"));
    }


}
