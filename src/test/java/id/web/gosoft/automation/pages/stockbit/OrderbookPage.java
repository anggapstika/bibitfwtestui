package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class OrderbookPage extends BasePageObject {
    public void clickOrderbookHeader() {
        clickOn(By.cssSelector(".orderbook-mainpage-header-left"));
    }

    public void scrollToCreateListBtn() {
        scrollIntoView(By.cssSelector(".newwl"));
    }

    public void clickCreateListBtn() {
        clickOn(By.cssSelector(".newwl"));
    }

    public void inputListName(String name) {
        typeOn(By.cssSelector("input.crtnewwl-name"), name);
    }

    public void clickConfirmCreateBtn() {
        clickOn(By.cssSelector(".mdl-btn.mdl-action"));
    }

    public String getOrderbookListHeader() {
        return getText(By.cssSelector(".orderbook-mainpage-title"));
    }

    public String getLatestOrderbookListName() {
        return getText(By.cssSelector(".orderbook-page-popover-item > div:last-child > span:first-child"));
    }

    public void clickSaveOrderbookListBtn() {
        clickOn(By.cssSelector(".save-orderbook"));
    }

    public void inputNewListName(String name) {
        typeOn(By.cssSelector("input.crtnewwl-name[placeholder='New orderbook name']"), name);
    }

    public void clickConfirmUpdateBtn() {
        String xpath = "//div[contains(text(), 'Change orderbook name')]"
                + "/parent::*/parent::*"
                + "//button[contains(@class, 'mdl-action')]";
        clickOn(By.xpath(xpath));
    }

    public void editLatestOrderbookList() {
        clickOn(By.cssSelector(".orderbook-page-popover-item > div:last-child > .icon-toolbar_pencil"));
    }

    public void deleteLatestOrderbookList() {
        clickOn(By.cssSelector(".orderbook-page-popover-item > div:last-child > .icon-trading_delete"));
    }

    public void clickConfirmDeleteBtn() {
        clickOn(By.cssSelector("input.modal-button.modal-save"));
    }

    public String getSuccessAlertMessage() {
        return getText(By.cssSelector(".sysmsg.alert-top.alert-green"));
    }

    public void clickAddOrderbookBtn() {
        clickOn(By.cssSelector(".add-orderbook"));
    }

    public void clickConfirmAddOrderbookBtn() {
        clickOn(By.cssSelector(".ant-btn.ant-btn-primary"));
    }

    public void inputOrderbookSymbol(String symbol) {
        typeOn(By.cssSelector("input.blank_orderbook"), symbol);
    }

    public void selectFirstSymbolSuggestion() {
        clickOn(By.cssSelector(".option.stock"));
    }

    public String getLatestOrderbookName() {
        return getAttribute(By.cssSelector(".orderbook-no-portfolio:last-child input"), "value");
    }

    public boolean isLatestTablePresent() {
        return isPresent(By.cssSelector(".orderbook-no-portfolio:last-child .orderbook-price-container"));
    }

    public boolean isSymbolSuggestionPresent() {
        return isPresent(By.cssSelector(".option.stock"));
    }

    public int getOrderbookListSize() {
        return findAll(By.cssSelector(".orderbook-no-portfolio")).size();
    }

    public void clickLatestOrderbookDropmenu() {
        clickOn(By.cssSelector(".orderbook-no-portfolio:last-child .dropmenu"));
    }

    public void clickRemoveOrderbook() {
        clickOn(By.xpath("//*[contains(text(), 'Remove')]/parent::*"));
    }

    public void clickMenu(String menu) {
        String xpath = "//*[@class='orderbook-right-popover-menu']//a[text()='"
                + menu
                + "']";
        clickOn(By.xpath(xpath));
    }

    public String getChartbitCompanyName() {
        return getText(By.xpath("//*[@class='pane-legend-title__description']"));
    }
}
