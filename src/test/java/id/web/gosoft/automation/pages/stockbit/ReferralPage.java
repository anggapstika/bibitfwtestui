package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;

//class LandingPage
public class ReferralPage extends BasePageObject {
    public void clickMenuReferral() {
        clickOn(By.cssSelector("a[href='#/refferal']"));
    }

    public void clickEditReferral() {
        clickOn(By.cssSelector(".content-refferal-edit button.green-text"));
    }

    public void inputReferral(String code) {
        typeOn(By.cssSelector("input#referralCode"), code);
    }

    public void clickSaveReferral() {
        clickOn(By.cssSelector(".ant-modal-content button[type='submit']"));
    }

    public String getSuccessAlertMessage() {
        return getText(By.cssSelector(".sysmsg.alert-top.alert-green"));
    }

    public void clickShareReferral() {
        clickOn(By.cssSelector("button.share"));
    }

    public boolean isModalShareReferralPresent() {
        return isPresent(By.cssSelector(".modal-share-code .social-media"));
    }

    public void clickCopyReferral() {
        clickOn(By.cssSelector("button.copy"));
    }

    public String getCurrentReferralCode() {
        return getText(By.cssSelector(".content-refferal-edit p"));
    }

    public String getCopiedReferralCode() {
        try {
            String referral = (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);

            return referral;
        } catch (Exception e) {
            return "";
        }
    }

    public void clickViewRedeemList() {
        clickOn(By.cssSelector(".securities-refferal-page-box-information a"));
    }

    public void clickRedeemTab(String tab) {
        clickOn(By.xpath("//*[contains(@class, 'securities-refferalFriends-page-tab')]//span[text()='"
                + tab
                + "']/parent::button"));
    }

    public void clickRedeemFirstCoupon() {
        clickOn(By.cssSelector(".securities-refferalFriends-page-friendList button"));
    }

    public boolean isAtChallengePinPage() {
        return isPresent(By.cssSelector("input#pin"));
    }

    public boolean isOnReferralHistoryPage() {
        return isPresent(By.cssSelector(".securities-refferalFriends-page-friendList")) &&
                !isPresent(By.cssSelector(".securities-refferalFriends-page-coupon"));
    }

    public boolean isShownModalTungguDulu() {
        return isPresent(By.xpath("//p[contains(text(), 'Tunggu Dulu')]"));
    }
}
