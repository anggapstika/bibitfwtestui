package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyPage extends BasePageObject {
    public void goToCompanyPage(String symbol) {
        String url = "https://stockbit.com/symbol/" + symbol;
        openAt(url);
    }

    public String getCompanySymbol() {
        return getText(By.cssSelector(".company-symbol b"));
    }

    public String getCompanyName() {
        return getText(By.cssSelector(".company-name>span"));
    }

    public boolean isTabActive(String name) {
        String[] tabs = find(By.xpath("//b[text()='" + name + "']/parent::a")).getAttribute("class").split(" ");
        return Arrays.asList(tabs).contains("active");
    }

    public void clickTabMore() {
        clickOn(By.cssSelector(".company-navigation-more"));
    }

    public void scrollToCatalogSection() {
        scrollIntoView(By.cssSelector(".widget-collection-head"));
    }

    public void clickCatalog(String catalog) {
        clickOn(By.xpath("//div[@class='widget-collection']//a[text()='" + catalog + "']"));
    }

    public void scrollToNavigation() {
        WebElement element = find(By.cssSelector(".company-navigation"));
        int y = element.getLocation().getY();
        scrollByPixels(0, y - 120);
    }

    public void clickNavTab(String tab) {
        By by = By.xpath("//b[text()='" + tab + "']//parent::a");
        if (!isClickable(by)) {
            by = By.xpath("//*[@class='company-dropdown']//b[text()='" + tab + "']//parent::a");
            clickTabMore();
        }
        clickOn(by);
    }

    public void clickChartIcon() {
//        clickOn(By.cssSelector("#mycontainer i"));
        clickOn(By.cssSelector("div.announcement-next:nth-child(2) div.content-wrapper:nth-child(5) div.content-box div.company-content:nth-child(3) div.keystat-group-box:nth-child(1) div.item-list tr:nth-child(1) td:nth-child(2) div.align-right a:nth-child(1) > i.icon-toolbar_fundachart"));
    }

    public boolean isFundachartPresent() {
        return isPresent(By.id("plotplace"));
    }

    public void setSeasonalityRange(String year) {
        typeOn(By.id("prependedInput"), year);
    }

    public void setSeasonalityEnd(String year) {
        typeOn(By.cssSelector(".a4.endyear"), year);
    }

    public void clickShowSeasonality() {
        clickOn(By.cssSelector(".a5.btn.btn-success"));
    }

    public List<String> getListYear() {
        return findAll(By.cssSelector(".headcol2.year")).stream()
                .map(WebElement::getText).collect(
                        Collectors.toList());
    }

    public void chooseFundachartStock(String stock) {
        typeOn(By.cssSelector("input[name='symbol']"), stock);
        clickOn(By.cssSelector(".option.stock"));
    }

    public void chooseFundachartMetric(String metric) {
        typeOn(By.cssSelector("input.fundachart"), metric);
        clickOn(By.cssSelector("#react-autowhatever-autosuggest-stocks li"));
    }

    public boolean isGraphDisplayed(String text) {
        return isPresent(By.xpath("//td[@class='legendLabel' and text()='" + text + "']"));
    }

    public void clickChartSave() {
        clickOn(By.cssSelector("a.save-fundachart"));
    }

    public void clickChartSaveAs() {
        clickOn(By.cssSelector("a.saveas"));
    }

    public void inputNewChartName(String name) {
        typeOn(By.cssSelector("input.modal-content-input"), name);
    }

    public void clickConfirmSave() {
        clickOn(By.cssSelector("input.modal-button.modal-save"));
    }

    public boolean isNewChartExists(String name) {
        return isPresent(By.xpath("//span[text() = '" + name + "']"));
    }

    public boolean isOnStreamSection() {
        return isPresent(By.cssSelector(".stream-compose.compose-box"));
    }

    public boolean isSymbolPresentInAll(String symbol) {
        List<WebElement> elementList = findAllElement(By.cssSelector(".stream-col-right")).subList(0, 5);

        for (WebElement element : elementList) {
            if (!isNested(element, By.xpath(".//a[contains(@class,'stock-indicator') and (text()='$" + symbol + "' or text()='$" + symbol.toLowerCase() + "')]")))
                return false;
        }
        return true;
    }

    public void inputComparisonStock(String stock) {
        typeOn(By.cssSelector("input.stocksearch"), stock);
    }

    public void clickFirstComparisonResult() {
        clickOn(By.cssSelector(".option.stock"));
    }

    public String getLatestComparisonStock() {
        return getText(By.cssSelector("thead tr th:last-child .dragtable-drag-handle"));
    }

    public void clickDeleteLatestComparisonStock() {
        clickOn(By.cssSelector("thead tr th:last-child a.toggle-vis"));
    }

    public void clickSaveComparison() {
        clickOn(By.cssSelector("a.save-comparison"));
    }

    public void clickSaveComparisonAs() {
        clickOn(By.cssSelector("a.saveas"));
    }

    public void inputNewComparisonName(String name) {
        typeOn(By.cssSelector("input.modal-content-input"), name);
    }

    public void clickConfirmSaveComparison() {
        clickOn(By.cssSelector("input.modal-save"));
    }

    public void scrollToLatestComparison() {
        scrollIntoView(By.cssSelector(".save-action-comparison span:nth-last-child(5)"));
    }

    public String getSuccessAlertMessage() {
        return getText(By.cssSelector(".sysmsg.alert-top.alert-green"));
    }

    public String getLatestSavedComparisonName() {
        return getText(By.cssSelector(".save-action-comparison > span:last-child > div"));
    }

    public void clickDeleteLatestComparison() {
        clickOn(By.cssSelector(".save-action-comparison > span:last-child a.delete-column"));
    }

    public boolean isOnConsensusPage(String title) {
        return title.equals(getText(By.cssSelector("th.info")));
    }

    public void hoverConsensusGraph() {
        mouseOver(By.cssSelector(".icon-toolbar_fundachart"));
    }

    public boolean isTooltipGraphDisplayed() {
        return isPresent(By.cssSelector(".tooltipster-box"));
    }

    public void clickCorpActionTab(String tab) {
//        clickOn(By.xpath("//b[text()='" + tab + "']//parent::div[contains(@class,'menu-item')]"));
        waitABit(5);
        clickOn(By.xpath("//div[@class='menu-item ']/b[text()='"+tab+"']"));
    }

    public boolean isCorpActionTabActive(String tab) {
        String[] tabs = find(By.xpath("//b[text()='" + tab + "']//parent::div[contains(@class,'menu-item')]"))
//        String[] tabs = find(By.xpath("//div[@class='menu-item ']/b[text()='"+tab+"']"))

                .getAttribute("class").split(" ");
        return Arrays.asList(tabs).contains("active");
    }

    public void clickInvestorProfile() {
        clickOn(By.cssSelector("tr:nth-child(1) td:nth-child(2) a"));
    }

    public boolean isInsiderExist(String symbol) {
        return isPresent(By.xpath("//div[contains(@class, 'insider-collapse') and text()='" + symbol + "']"));
    }

    public void clickBuy() {
        clickOn(By.cssSelector("a.trade"));
    }

    public void clickNavigate(){ clickOn(By.cssSelector(".company-navigation-more"));}

    public void clickCorporateAction() {clickOn(By.cssSelector(".div.announcement-next:nth-child(2) div.content-wrapper.navdown:nth-child(5) div.content-box div.company-navigation-wrap.nav9:nth-child(2) div.company-navigation.navopen div.company-dropdown:nth-child(15) a.navnav.drophide:nth-child(5) > b:nth-child(1)"));}
}
