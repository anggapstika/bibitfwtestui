package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class ProfilePage extends BasePageObject {
    public void clickEditProfile() {
        clickOn(By.className("editprofile"));
    }

    public void clickEditProfileTab() {
        clickOn(By.cssSelector("a[data-setting='setting-profile']"));
    }

    public void inputName(String name) {
        typeOn(By.name("fullname"), name);
    }

    public void inputEmail(String email) {
        typeOn(By.name("email"), email);
    }

    public void clickCheckboxHideEmail() {
        By by = By.id("hideEmail");
        WebElement checkBox = find(by);

        clickOn(by);
        if (!checkBox.isSelected()) {
            clickOn(by);
        }
    }

    public void inputBiography(String bio) {
        typeOn(By.name("about"), bio);
    }

    public void selectLocation(String location) {
        select(By.name("location"))
                .selectByVisibleText(location);
    }

    public void selectGender(String gender) {
        select(By.name("gender"))
                .selectByVisibleText(gender);
    }

    public void selectDay(String day) {
        select(By.name("day"))
                .selectByVisibleText(day);
    }

    public void selectMonth(String month) {
        select(By.name("month"))
                .selectByVisibleText(month);
    }

    public void selectYear(String year) {
        select(By.name("year"))
                .selectByVisibleText(year);
    }

    public void inputWebsite(String web) {
        typeOn(By.name("website"), web);
    }

    public void inputAddress(String address) {
        typeOn(By.name("address"), address);
    }

    public void clickSaveSetting() {
//        clickOn(By.className("settting-save-btn"));
        clickOn(By.cssSelector(".settting-save-btn"));
//        clickOn(By.cssSelector(".ok"));

    }

    public String getEditName() {
        return getAttribute(By.name("fullname"), "value");
    }

    public String getEditEmail() {
        return getAttribute(By.name("email"), "value");
    }

    public boolean isHideEmailChecked() {
        return find(By.id("hideEmail")).isSelected();
    }

    public String getEditBiography() {
        return getText(By.name("about"));
    }

    public String getEditLocation() {
        return select(By.name("location"))
                .getFirstSelectedOption()
                .getText();
    }

    public String getEditGender() {
        return select(By.name("gender"))
                .getFirstSelectedOption()
                .getText();
    }

    public String getEditDay() {
        return select(By.name("day"))
                .getFirstSelectedOption()
                .getText();
    }

    public String getEditMonth() {
        return select(By.name("month"))
                .getFirstSelectedOption()
                .getText();
    }

    public String getEditYear() {
        return select(By.name("year"))
                .getFirstSelectedOption()
                .getText();
    }

    public String getEditWebsite() {
        return getAttribute(By.name("website"), "value");
    }

    public String getEditAddress() {
        return getText(By.name("address"));
    }

    public void clickEditPreferencesTab() {
//        clickOn(By.cssSelector("a[data-setting='setting-preferences']"));
        clickOn(By.xpath("//div[@class='sidebar']//child::div[2]"));

    }

    public void pickRegion(String region) {
        String selector;
        switch (region) {
            case REGION_INDONESIA:
                selector = "label[for='countryID']";
                break;
            case REGION_MALAYSIA:
                selector = "label[for='countryMY']";
                break;
            default:
                selector = "";
        }
        clickOn(By.cssSelector(selector));
    }

    public boolean isRegionSelected(String region) {
        String selector;
        switch (region) {
            case REGION_INDONESIA:
                selector = "//*[@for='countryID']//parent::*";
                break;
            case REGION_MALAYSIA:
                selector = "//*[@for='countryMY']//parent::*";
                break;
            default:
                selector = "";
        }

        return Arrays.asList(find(By.xpath(selector))
                .getAttribute("class").split(" "))
                .contains("checked");
    }

    public void clickEditPasswordTab() {
//        clickOn(By.cssSelector("a[data-setting='setting-password']"));
        clickOn(By.xpath("//div[@class='sidebar']//child::div[4]"));
    }

    public void inputOldPassword(String password) {
//        typeOn(By.name("old_password"), password);
        typeOn(By.cssSelector(".ant-input"), password);
    }

    public void inputNewPassword(String password) {
//        typeOn(By.name("new_password"), password);
//        typeOn(By.xpath("//div[@class='content-form']//child::div[2]/div[1]"), password);
        typeOn(By.xpath("//div[@class='content-form']//child::div[1]/div[2]/div/input"),password);
    }

    public void inputVerifyPassword(String password) {
//        typeOn(By.name("confirm_new_password"), password);
        typeOn(By.xpath("//div[@class='content-form']//child::div[2]/div[2]/div/input"), password);
    }

    public boolean isPasswordUpdatedAlertShown() {
//        return isPresent(By.xpath("//*[text()='Password Changed']"));
        return isPresent(By.cssSelector(".profileright"));
    }

    public boolean isIncorrectCurrentPasswordAlertShown() {
//        return isPresent(By.xpath("//*[text()='Your current password is incorrect']"));
        return isPresent(By.cssSelector(".icon-toolbar_error"));
    }

    public boolean isNewPasswordTooShortShown() {
        return isPresent(By.xpath("//*[text()='new harus terdiri dari minimal 6 karakter']"));
    }

    public boolean isVerifyPasswordTooShortShown() {
        return isPresent(By.xpath("//*[text()='verify harus terdiri dari minimal 6 karakter']"));
    }

    public boolean isPasswordNotMatchShown() {
//        return isPresent(By.xpath("//*[text()='Password does not match']"));
        return isPresent(By.cssSelector(".icon-toolbar_error"));
    }

    public void clickEditNotificationsTab() {
//        clickOn(By.cssSelector("a[data-setting='setting-notification']"));
        clickOn(By.xpath("//div[@class='sidebar']//child::div[5]"));
    }

    private int getSettingNotifIndex(String type) {
        switch (type) {
            case NOTIF_ACCORDION_EMAIL:
                return 1;
            case NOTIF_ACCORDION_MOBILE:
                return 2;
            case NOTIF_ACCORDION_DESKTOP:
                return 3;
            default:
                return 0;
        }
    }

    public void clickNotifAccordion(String type) {
        int idx = getSettingNotifIndex(type);
        clickOn(By.cssSelector(".react-sanfona-item.hehe:nth-child(" + idx + ")"));
    }

    public void clickNotifCheckbox(String type, String checkbox, boolean targetStatus) {
        int idx = getSettingNotifIndex(type);

        By by = By.xpath("//*[contains(@class, 'react-sanfona-item hehe')][" + idx + "]" +
                "//*[@class='control-label' and text()='" + checkbox + "']" +
                "/following-sibling::*//input");

        scrollIntoView(by);
        scrollByPixels(0, -100);

        WebElement checkBox = find(by);

        clickOn(by);
        if (checkBox.isSelected() != targetStatus) {
            clickOn(by);
        }
    }

    public boolean isNotifChecked(String type, String checkbox) {
        int idx = getSettingNotifIndex(type);

        By by = By.xpath("//*[contains(@class, 'react-sanfona-item hehe')][" + idx + "]" +
                "//*[@class='control-label' and text()='" + checkbox + "']" +
                "/following-sibling::*//input");

        scrollIntoView(by);
        scrollByPixels(0, -100);

        WebElement checkBox = find(by);
        return checkBox.isSelected();
    }

    public void clickEditPrivacyTab() {
//        clickOn(By.cssSelector("a[data-setting='setting-privacy']"));
        clickOn(By.xpath("//div[@class='sidebar']//child::div[7]"));
    }

    public void clickSwitchDiscoverableByContacts(boolean targetStatus) {
        By by = By.className("socialAccount");
        clickOn(by);

        waitABit(3);

        boolean isSwitchOn = Arrays.asList(find(by)
                .getAttribute("class").split(" "))
                .contains("on");

        if (isSwitchOn != targetStatus) {
            clickOn(by);
        }
    }

    public boolean isSwitchDiscoverableByContactsActive() {
        return Arrays.asList(find(By.className("socialAccount"))
                .getAttribute("class").split(" "))
                .contains("on");
    }

    public void blockUser() {
        clickOn(By.className("profile-more"));
        clickOn(By.cssSelector("li.ant-dropdown-menu-item:last-of-type"));
        clickOn(By.cssSelector("input.save"));
    }

    public void clickEditBlockedListTab() {
//        clickOn(By.cssSelector("a[data-setting='setting-blocked']"));
        clickOn(By.xpath("//div[@class='sidebar']//child::div[8]"));
    }

    public void clickUnblockUser(String user) {
        clickOn(By.cssSelector("a[href='#/" + user + "'] + .unblock"));
        clickOn(By.cssSelector("input.save"));
    }

    public boolean isUserUnblocked() {
        return isPresent(By.cssSelector(".sysmsg.alert-green"));
    }

    public void userClickThreeDotsProfile(){ clickOn(By.cssSelector(".profile-more"));}

    public void clickBlock() { clickOn(By.cssSelector(".ant-dropdown-menu li:nth-of-type(4)"));}

    public void clickBlockOnPopup() { clickOn(By.xpath("//input[@value='Block']"));}

    public String getNotifBlock() {
        return getText(By.xpath("//div[@class='sysmsg-content']/div"));
    }

    public boolean isVerifiedBadgeDisplayedOnPost(){
        return isDisplayed(By.cssSelector(".icon-new-verified-profile"));
    }

    public void clickShowAll(){
        clickOn(By.cssSelector(".show-all"));
    }

    public void clickRandomAvatar(){
        clickOn(By.cssSelector(".ant-modal-content ul.avatar-lists li:nth-of-type(1) img"));
    }

    public void clickSave(){
        clickOn(By.cssSelector(".confirm-btn"));
    }

    public void chooseMale() {
        clickOn(By.cssSelector(".ant-modal-content .ant-select-selection-selected-value"));
        waitABit(3);
        clickOn(By.xpath("//ul/li[text()='Male']"));
    }

    public void chooseFemale(){
        clickOn(By.cssSelector(".ant-modal-content .ant-select-selection-selected-value"));
        waitABit(3);
        clickOn(By.xpath("//ul/li[text()='Female']"));
    }

    public void clickRemoveProfile(){
        clickOn(By.cssSelector(".remove-btn"));
    }

    public void clickRemoveAvatar(){
        clickOn(By.cssSelector(".remove-avatar-confirmation .confirm-btn"));
    }

    public boolean IsAvatarDisplayed(){
        return isPresent(By.cssSelector(".popavatar"));
    }

    public void clickEditSimbol(){
        clickOn(By.cssSelector(".icon-form-edit"));
    }

    public void clickPublicProfile(){
        clickOn(By.xpath("//div[@class='sidebar']//child::div[2]"));
    }

    public void clickBackProfileView(){
        clickOn(By.cssSelector(".icon-arrow-left"));
    }

    public void clickSubmitOldPassword(){
        clickOn(By.cssSelector(".ok"));
    }

    public void clickEmailNotification(){
        clickOn(By.xpath("//div/h3[text()='Email']"));
    }

    public void clickMobileNotification(){
        clickOn(By.xpath("//div/h3[text()='Mobile']"));
    }

    public void clickDekstopNotification(){
        clickOn(By.xpath("//div/h3[text()='Desktop']"));
    }

    public void clickProfileIcon(){
        clickOn(By.cssSelector(".avatar-wrapper"));
    }







}
