package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;

public class StreamPage extends BasePageObject {
    public void clickCompose() {
        clickOn(By.cssSelector(".stream-compose-write"));
    }

    public void writePost(String word) {
        typeOn(By.id("composetextarea"), word);
    }

    public void clickPostIdea() {
        clickOn(By.cssSelector(".compose-submit-button"));
    }

    public String getFirstPostText() {
//        return getText(By.className("stream-text-single"));
//        return getText(By.xpath("//*[@id=\"stimeline\"]/div/div[1]/div[4]/div[3]"));
        return getText(By.xpath("//*[@id=\"stimeline\"]//child::div[4]/div[3]"));

    }

    public String getFirstPostText2() {
        return getText(By.className("stream-text-single"));
    }

    public void clickViewPostComment() {
        clickOn(By.cssSelector(".stream-convo-amp"));
    }

    public void writeComment(String comment) {
        clickOn(By.cssSelector(".reply-box-before"));
        clickOn(By.cssSelector("#replytextarea"));
        waitABit(3);
        typeOn(By.cssSelector("#replytextarea"), comment);
    }

    public void clickPostComment() {
        clickOn(By.cssSelector(".reply-submit-button"));
    }

    public String getLatestComment() {
        return getText(By.cssSelector("#conversation-child .stream-box:last-child .stream-text"));
    }

    public void clickTargetIcon() {
        clickOn(By.cssSelector(".set-target-price-wrapper"));
    }

    public void inputTargetSymbol(String symbol) {
        typeOn(By.cssSelector("input.jsp-settarget-stock"), symbol);
    }

    public void clickFirstStockResult() {
        clickOn(By.cssSelector(".option.stock"));
    }

    public void inputTargetPrice(String price) {
        typeOn(By.id("setTargetPricePriceInput"), price);
    }

    public void selectTargetDuration(String duration) {
        select(By.cssSelector(".jsp-settarget-duration"))
                .selectByVisibleText(duration);
    }

    public String getIdeaTargetSymbol() {
        return getText(By.cssSelector(".STPwrap.STPstockstarget h3"));
    }

    public void clickLatestStockIndicator() {
        clickOn(By.cssSelector("a.stock-indicator"));
    }

    public boolean isPostContainsSymbol(String symbol) {
        return getText(By.cssSelector(".stream-box:first-child .stream-text")).contains(symbol);
    }

    public boolean isPostContainSymbolUrl() {
        return isPresent(By.cssSelector(".stream-box:first-child .stream-text a.stock-indicator"));
    }

    public void clickPollIcon() {
        clickOn(By.cssSelector(".chat-polling-stream"));
    }

    public void clickAddPollChoice() {
//        clickOn(By.cssSelector(".green-bibit a"));
        clickOn(By.xpath("//div[@class='green-bibit']/a"));
    }

    public void inputChoiceAtIndex(int idx, String text) {
        typeOn(By.cssSelector("input[placeholder='Choice " + idx), text);
//        String textId = "input[placeholder='Choice " + idx;
//        typeOn(By.id(textId), text);
    }


    public void setPollEndDate(String days, String hours, String minutes) {
        clickOn(By.cssSelector(".green-bibit:last-child a"));

        select(By.cssSelector("select[name='day']"))
                .selectByValue(days);
        select(By.cssSelector("select[name='hour']"))
                .selectByValue(hours);
        select(By.cssSelector("select[name='minute']"))
                .selectByValue(minutes);

        clickOn(By.cssSelector(".poll-time-btn a"));
    }

    public String getNthPollOptionText(int idx) {
        return getText(By.cssSelector(".stream-box:first-child .polling-container button.polling-choice:nth-child(" + idx + ")"));
    }

    public boolean isPollTimeLimitTheSame(String time) {
        return getText(By.cssSelector(".stream-box:first-child .polling-container .voter")).contains(time);
    }

    public void clickGifIcon() {
        clickOn(By.cssSelector(".chat-gif-stream"));
    }

    public void inputGifQuery(String query) {
        typeOn(By.cssSelector("input.stream-gif-search"), query);
    }

    public void clickFirstGifResult() {
        clickOn(By.cssSelector(".stream-gif-container .modal-gif-item"));
    }

    public String getFirstGifResultSrc() {
        return getAttribute(By.cssSelector(".stream-gif-container .modal-gif-item"), "src");
    }

    public boolean isGifPost() {
        return isPresent(By.cssSelector(".stream-box:first-child .stream-figure-img.gif"));
    }

    public String getPostedGifSrc() {
        return getAttribute(By.cssSelector(".stream-box:first-child .stream-figure-img.gif"), "src");
    }

    public void selectFile() {
        // Path for BrowserStack
//        String key = "C:\\Users\\hello\\Documents\\documents\\pdf-sample1.pdf";

        // Local path
        String key = "C:\\Users\\Bryan\\Documents\\sample.pdf";
        find(By.cssSelector("input[type='file'].compose-documentfile-hidden")).sendKeys(key);
    }

    public boolean isFilePostCreated() {
        return isPresent(By.cssSelector(".stream-box:first-child .file-attach.file-pdf"));
    }

    public void selectImage() {
        // Path for BrowserStack
//        String key = "C:\\Users\\hello\\Documents\\images\\wallpaper1.jpg";

        // Local path
        String key = "C:\\Users\\Bryan\\Pictures\\image.jpg";
        find(By.cssSelector("input[type='file'].compose-file-hidden")).sendKeys(key);
    }

    public boolean isImagePostCreated() {
        return isPresent(By.cssSelector(".stream-box:first-child .stream-figure-img"));
    }

    public boolean isPostLiked() {
        return isPresent(By.cssSelector(".stream-box:first-child .icon-new-likefill"));
    }

    public void clickLikePost() {
        clickOn(By.cssSelector(".stream-box:first-child .stream-like-amp .stream-icon"));
    }

    public void clickLikerList() {
        clickOn(By.cssSelector(".stream-box:first-child .stream-new-action-top-left"));
    }

    public String getLikerUsername() {
        return getText(By.cssSelector(".follow-bio a"));
    }

    public void clickNotificationIcon() {
        clickOn(By.cssSelector(".headermenu.notif .ant-dropdown-link"));
    }

    public boolean isAllNotificationVisible() {
        return isVisible(By.cssSelector(".notification-group-box.notification"));
    }

    public String getNotificationSectionName() {
        return getText(By.cssSelector(".notification-title"));
    }

    public void clickActivityNotifIcon() {
        clickOn(By.cssSelector(".icon-notification_activity"));
    }

    public boolean isActivityNotificationVisible() {
        return isVisible(By.cssSelector(".notification-group-box.activity"));
    }

    public void clickMentionNotifIcon() {
        clickOn(By.cssSelector(".icon-notification_mention"));
    }

    public boolean isMentionNotificationVisible() {
        return isVisible(By.cssSelector(".notification-group-box.mention"));
    }

    public void clickReportNotifIcon() {
        clickOn(By.cssSelector(".icon-notification_report"));
    }

    public boolean isReportNotificationVisible() {
        return isVisible(By.cssSelector(".notification-group-box.report"));
    }

    public void clickTippingNotifIcon() {
        clickOn(By.cssSelector(".icon-notification_tip"));
    }

    public boolean isTippingNotificationVisible() {
        return isVisible(By.cssSelector(".notification-group-box.tipping"));
    }

    public void waitUntilStreamIsLoaded() {
        waitUntilPresent(By.cssSelector(".stream-share-amp"));
    }

    public void clickStreamNav(String section) {
        clickOn(By.xpath("//*[@class='stream-nav']//*[text()='" + section + "']"));
    }

    public boolean isStreamNavActive(String section) {
        return isPresent(By.xpath("//*[@class='stream-nav']//*[text()='"
                + section
                + "']/parent::a[contains(@class, 'active')]"));
    }

    public boolean isShownNewsPost() {
        return isPresent(By.cssSelector("a.topuser"));
    }

    public boolean isShownReportsPost() {
        return isPresent(By.cssSelector("b[data-username='StockbitReports']"));
    }

    public boolean isShownPredictionsPost() {
        return isPresent(By.cssSelector(".STPwrap.STPstockstarget"));
    }

    public boolean isShownChartsPost() {
        return isPresent(By.cssSelector("img.stream-figure-img"));
    }

    public boolean isShownInsidersPost() {
        return isPresent(By.cssSelector("b[data-username='InsiderNews']"));
    }

    public void clickShareFirstPost() {
        clickOn(By.cssSelector(".stream-box:first-child a.stream-share-amp img"));
    }

    public void clickShareFirstPostStream(){
        clickOn(By.cssSelector(".stream-box:first-child a.stream-share-amp img"));
//        clickOn(By.xpath("(//div[@class='stream-new-action']/a[@class='stream-share-amp'])[1]"));
    }

    public void clickShareFirstPolling(){
        clickOn(By.cssSelector(".stream-box:first-child a.stream-share-amp img"));
    }

    public void clickCopyLink() {
        clickOn(By.cssSelector(".share-to-icon-link"));
    }

    public String getFirstPostId() {
        return getAttribute(By.cssSelector(".stream-box:first-child a.stream-like-amp"), "data-postid");
    }

    public String getCopiedPostId() {
        try {
            String copiedPostId = (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
            int idx = copiedPostId.lastIndexOf("/") + 1;

            return copiedPostId.substring(idx);
        } catch (Exception e) {
            return "";
        }
    }

    public String getRepostedText() {
        return getText(By.cssSelector(".stream-repost-quote .stream-text"));
    }

    public void clickRepost() {
        clickOn(By.xpath("//div[@class='column right']"));
    }

    public void inputMessageRecipient(String name) {
        typeOn(By.cssSelector(".input-search-friends"), name);
    }

    public void userClickMore(){clickOn(By.xpath("//div[@class='stream-avatar last-contacted more']"));}

    public void clickFirstRecipient() {
        clickOn(By.cssSelector(".share-group-suggestion"));
        waitABit(5);
        clickOn(By.cssSelector(".ant-checkbox-inner"));
    }

    public String getSharedMessage() {
//        return getAttribute(By.cssSelector("textarea.share-chat-textarea"), "value");
        return getAttribute(By.xpath("//textarea[@class='ant-input share-socmed-comment']"), "value");
    }

    public String getSharedMessage2() {
        return getText(By.cssSelector(".message.chat:last-of-type .chat-content > div"));
    }

    public void clickSendMessage() {
//        clickOn(By.cssSelector("button.ant-btn-primary"));
        clickOn(By.cssSelector(".button-submit-comment"));
    }

    public void scrollToMainNavigation() {
        WebElement element = find(By.cssSelector(".nav-inside"));
        int y = element.getLocation().getY();
        scrollByPixels(0, y - 2000);
    }

    public void scrollToProfileStream() {
        WebElement element = find(By.cssSelector(".polling"));
        int y = element.getLocation().getY();
        scrollByPixels(0, y + 500);
    }

    public void scroolToStreamPost() {
        WebElement element = find(By.cssSelector(".stream-box"));
        int y = element.getLocation().getY();
        scrollByPixels(0, y + 200);
    }


    public String getTimestamp() {
        return getText(By.cssSelector(".voter"));
    }

    public void clickCloseSharePopup(){ clickOn(By.cssSelector(".ant-modal-close-x"));}

    public void inputLinkYouTube(String linkYoutube){
        typeOn(By.id("composetextarea"),linkYoutube);
    }

    public void writeMessage(String message) {
        typeOn(By.xpath("//textarea[@class='ant-input share-socmed-comment']"), message);
    }

    public void clickNewsTab(){ clickOn(By.xpath("//span[.='News']"));}

    public void clickReportsTab(){ clickOn(By.xpath("//span[.='Reports']"));}

    public void clickPredictionsTab() { clickOn(By.xpath("//span[.='Predictions']"));}

    public void clickChartsTab() { clickOn(By.xpath("//span[.='Charts']"));}

    public void isNoComment(){
        isDisplayed(By.cssSelector("#conversation-child"));
    }

    public void clickCloseCommentPopup(){
        clickOn(By.cssSelector(".icon-compose_close"));
    }

    public String getNotifLinkCopied(){ return getText(By.xpath("//div[@class='sysmsg-content']/div"));}

    public void clickAyoDaftar() {clickOn(By.cssSelector(".googposter"));}

    public void clickLanscapeAd() {clickOn(By.cssSelector(".bibitads-wrap"));}

    public boolean userAtSekuritasRegistrationPage(){
        return isDisplayed(By.cssSelector(".title-register"));
    }


}
