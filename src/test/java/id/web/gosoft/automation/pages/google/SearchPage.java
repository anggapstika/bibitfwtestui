package id.web.gosoft.automation.pages.google;

import id.web.gosoft.automation.pages.base.BasePageObject;
import id.web.gosoft.automation.webdriver.WebdriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class SearchPage extends BasePageObject {

  public void openPage() {
    getDriver().get("https://www.google.com");
  }

  public void search(String keyword) {
    WebElement inputSearch = getDriver()
        .findElement(By.name("q"));
    inputSearch.sendKeys(keyword + Keys.ENTER);
  }

}
