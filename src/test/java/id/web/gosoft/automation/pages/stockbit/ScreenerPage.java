package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.stream.Collectors;

public class ScreenerPage extends BasePageObject {
    public void clickPresetScreenerBtn() {
        clickOn(By.className("prst-screener-btn"));
    }

    public void inputPresetScreener(String query) {
        typeOn(By.cssSelector("input.preset-screener-search"), query);
    }

    public void chooseFirstScreenerSearchResult() {
        clickOn(By.cssSelector("li[role='option']"));
    }

    public String getScreenerName() {
        return getAttribute(By.cssSelector("input.fullwidth.typical.typad"), "value");
    }

    public void clickCreateScreenerBtn() {
        clickOn(By.className("add-prst-screener"));
    }

    public void inputScreenerName(String name) {
        typeOn(By.cssSelector("input.fullwidth.typical.typad"), name);
    }

    public void openSelectStockUniverse() {
        clickOn(By.cssSelector(".stock_universe.ant-select"));
    }

    public void selectStockUniverse(String stock) {
        WebElement lastElement = null;
        String tempText;
        String lastText = "";

        outerLoop:
        while (true) {
            List<WebElement> elementList = findAllElement(By.cssSelector(".ant-select-tree-node-content-wrapper"));

            // look for stock universe with the same name
            for (int i = 0; i < elementList.size(); i++) {
                WebElement element = elementList.get(i);
                String txt = element.getAttribute("title");
                if (txt.equalsIgnoreCase(stock)) {
                    element.click();
                    break outerLoop;
                }
            }

            WebElement temp = elementList.get(elementList.size() - 1);
            tempText = temp.getAttribute("title");
            if (lastElement != null) {
                // stop scrolling if the bottom-most text is the same with the previous loop
                if (tempText.equals(lastText)) break;
            }

            lastElement = temp;
            lastText = tempText;

            scrollByPixels(By.className("ant-select-dropdown"), 0, 300);
        }
    }

    public void removeDefaultRule() {
        WebElement el = find(By.cssSelector("a.remove-rule"));

        Actions act = new Actions(getDriver());
        act.moveToElement(el).moveByOffset(20, 0).click().perform();
    }

    public void clickAddRuleBtn() {
        clickOn(By.cssSelector("button.addrule-btn"));
    }

    public void clickAddBasicRatio() {
        clickOn(By.cssSelector(".addrule>div:first-child"));
    }

    private int getLatestRuleIdx() {
        return findAll(By.cssSelector(".rule-list > div")).size();
    }

    public void inputFirstFinancialMetric(String metric) {
        typeOn(By.cssSelector(".rule-list > div:nth-of-type("
                + getLatestRuleIdx()
                + ") .autosuggest-general:nth-of-type(1) .screenerrules"), metric);

        clickOn(By.cssSelector("li.react-autosuggest__suggestion"));
    }

    public void selectOperator(String op) {
        select(By.cssSelector(".rule-list > div:nth-of-type("
                + getLatestRuleIdx()
                + ") select[name='operator']"))
                .selectByValue(op);
    }

    public void inputBasicValue(String value) {
        typeOn(By.cssSelector(".rule-list > div:nth-of-type("
                + getLatestRuleIdx()
                + ") input.wd4x"), value);
    }

    public void inputRatioValue(String value) {
        typeOn(By.cssSelector(".rule-list > div:nth-of-type("
                + getLatestRuleIdx()
                + ") input.wd4"), value);
    }

    public void clickAddRatioVsRatio() {
        clickOn(By.cssSelector(".addrule>div:last-child"));
    }

    public void inputSecondFinancialMetric(String metric) {
        typeOn(By.cssSelector(".rule-list > div:nth-of-type("
                + getLatestRuleIdx()
                + ") .autosuggest-general:nth-of-type(2) .screenerrules"), metric);

        clickOn(By.cssSelector("li.react-autosuggest__suggestion"));
    }

    public void clickSaveScreener() {
        clickOn(By.cssSelector("button.save-rule"));
    }

    public void openFavouriteScreenerMenu() {
        clickOn(By.cssSelector(".screener-more"));
    }

    public void selectNewestScreener() {
        scrollToBottomOfDiv(By.cssSelector(".wrapper-screener-myfavorite"));
        clickOn(By.cssSelector(".wrapper-screener-allscreener .item-list-myscreener:last-of-type"));
    }

    public List<String> getScreenerMetrics() {
//        return findAll(By.cssSelector(".dragtable-drag-handle"))
        return findAll(By.xpath("//div[@class='wrap-inner']//child::th"))

                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public String getLatestSavedScreenerName() {
        scrollToBottomOfDiv(By.cssSelector(".wrapper-screener-myfavorite"));
        return getText(By.cssSelector(".wrapper-screener-allscreener .item-list-myscreener:last-of-type i:first-child"));
    }

    public void deleteLatestScreener() {
        scrollToBottomOfDiv(By.cssSelector(".wrapper-screener-myfavorite"));
        clickOn(By.cssSelector(".item-list-myscreener:last-of-type .icon-trading_delete"));
        clickOn(By.cssSelector(".modal-button.modal-save"));
    }

    public void removeFavouritedScreener(String name) {
        WebElement lastElement = null;
        String tempText;
        String lastText = "";

        scrollToTopOfDiv(By.className("wrapper-more-screener"));

        outerLoop:
        while (true) {
            List<WebElement> elementList = findAllElement(By.cssSelector(".wrapper-screener-myfavorite .item-list-myscreener"));

            // look for favourited preset with the same name
            for (int i = 0; i < elementList.size(); i++) {
                WebElement element = elementList.get(i);
                String txt = element.findElement(By.xpath("./i[1]")).getText();
                if (txt.equalsIgnoreCase(name)) {
                    element.findElement(By.xpath("./i[2]")).click();
                    break outerLoop;
                }
            }

            WebElement temp = elementList.get(elementList.size() - 1);
            tempText = temp.findElement(By.cssSelector("i:first-child")).getText();
            if (lastElement != null) {
                // stop scrolling if the bottom-most text is the same with the previous loop
                if (tempText.equals(lastText)) break;
            }

            lastElement = temp;
            lastText = tempText;

            scrollByPixels(By.className("wrapper-more-screener"), 0, 300);
        }
    }

    public void choosePresetAtLevel(int level, String name) {
        clickOn(By.xpath("//li[contains(@class, 'level"
                + level
                + "') and text()='"
                + name
                + "']"));
    }

    public void favoritedPresetScreener(String name) {
        clickOn(By.xpath("//li[contains(@class, 'level3')]/b[text()='"
                + name
                + "']/following-sibling::*"));
    }

    public void closePresetScreenerMenu() {
        clickOn(By.cssSelector("button.ant-modal-close"));
    }

    public boolean isScreenerFavorited(String name) {
        return isPresent(By.xpath("//*[@class='wrapper-screener-myfavorite']//i[1 and text()='"
                + name
                + "']"));
    }

    public void clickEditScreener() {
        clickOn(By.cssSelector("button.save-rule"));
    }

    public void removeLatestRule() {
        WebElement el = find(By.cssSelector(".rule-list > .rule:last-child a.remove-rule"));

        Actions act = new Actions(getDriver());
        act.moveToElement(el).moveByOffset(20, 0).click().perform();
    }

    public void clickAddFinancialColumnBtn() {
        clickOn(By.id("wladdcolumn"));
    }

    public void inputAddFinancialColumn(String query) {
        typeOn(By.cssSelector("input.metric-search"), query);
    }
}
