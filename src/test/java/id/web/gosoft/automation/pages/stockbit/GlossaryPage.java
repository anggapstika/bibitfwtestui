package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class GlossaryPage extends BasePageObject {
    public void inputGlossarySearch(String glossary) {
        typeOn(By.cssSelector("input[name='glossary']"), glossary);
    }

    public void clickFirstSearchResult() {
        clickOn(By.cssSelector("li[role='option']:first-child"));
    }

    public String getActiveGlossaryIndex() {
        return getText(By.cssSelector(".gloss-index li.active a"));
    }

    public String getActiveGlossaryRow() {
        return getText(By.cssSelector(".glossary-row a.active"));
    }

    public String getSearchGlossary() {
        return getText(By.cssSelector("#term_result .glossary-title"));
    }

    public void clickGlossaryIndex(String index) {
        index = index.toLowerCase();
        clickOn(By.cssSelector("a[href='#/glossary/letter/" + index + "']"));
    }

    public List<String> getListGlossaryRowFirstLetter() {
        return findAll(By.cssSelector(".glossary-row a"))
                .stream()
                .map(el -> el.getText().substring(0, 1))
                .collect(Collectors.toList());
    }
}
