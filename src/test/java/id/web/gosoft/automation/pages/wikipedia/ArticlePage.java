package id.web.gosoft.automation.pages.wikipedia;

import id.web.gosoft.automation.pages.base.BasePageObject;
import id.web.gosoft.automation.webdriver.WebdriverInstance;
import org.openqa.selenium.By;


public class ArticlePage extends BasePageObject {
  public String getPokemonNumber() {
    return getDriver().findElement(By.xpath("//big//abbr")).getText();
  }

  public String getPokemonName() {
    return getDriver().findElement(By.id("firstHeading")).getText();
  }

}
