package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class OrderPage extends BasePageObject {
    String inputPriceXpath = "//*[text()='Price']/following-sibling::*//input";

    public void clickStartTrading() {
        clickOn(By.cssSelector(".trading-nonlogin"));
    }

//    public void clickVirtualTrading() {
//        clickOn(By.cssSelector("a.virtual-button"));
//    }

    public void clickPortffolioDropdown() {
//        clickOn(By.cssSelector(".action-setting.ant-dropdown-trigger"));
//        clickOn(By.xpath("//tbody[@class='ant-table-tbody']//child::tr[2]/td[10]//button/i"));
        clickOn(By.cssSelector(".icon-trading_gear"));
    }

    public void clickBuyMore() {
        clickOn(By.cssSelector("li.ant-dropdown-menu-item"));

    }

    public void clickAddLot() {
        clickOn(By.xpath("//div[@class='sellbuycontent']//child::div[5]/div[2]/div/div/span[1]"));
    }

    public void clickPlaceOrder() {
        clickOn(By.cssSelector(".modalorderbook-box .setorder"));
    }

    public void clickFirstTrendingStock() {
        clickOn(By.cssSelector("a.trending-box"));
    }

    public void clickTradingNavIdx(int idx) {
        clickOn(By.cssSelector(".tradingnav:nth-child(" + idx + ")"));
    }

    public String getMostRecentStatus() {
        return getText(By.cssSelector(".ant-table-tbody .td-mobile.status span"));
    }

    public void clickBuyFromPrediction() {
        clickOn(By.cssSelector(".STPbtnBuy.removePaddingLR a"));
    }

    public void clickBuyFromWatchlist() {
        clickOn(By.cssSelector(".ant-table-fixed-left a.wl-trade-buy-button"));
    }

    public void setOrderLot(String lot) {
        typeOn(By.cssSelector("input#ordbk_input_buy"), lot);
    }

    public boolean isOrderWarningPresent() {
        return isPresent(By.cssSelector(".order-message"));
    }

    public void inputPrice(String price) {
        By by = By.xpath("//*[text()='Price']/following-sibling::*//input");
//        By by = By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div[1]/div[2]/input");
        find(by).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        typeOn(by, price);
//        typeOn(By.xpath("//*[text()='Price']/following-sibling::*//div/div[2]"), price);
        }

    public void inputPriceReal(String price) {
        By by = By.xpath("//*[text()='Price']/following-sibling::*//input");
//        By by = By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div[1]/div[2]/input");
        find(by).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        typeOn(by, price);
//        typeOn(By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div[1]/div[2]/input"), price);
    }

    public void inputPriceToSell(String price){
        By by = By.xpath("//div[@class='sellbuycontent']//child::div[5]/div[2]/div//input");
        find(by).sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        typeOn(by, price);
//        typeOn(By.xpath("//div[@class='sellbuycontent']//child::div[5]/div[2]/div//input"),price);
    }


    private String getDefaultOfferPrice() {
        return find(By.xpath("//*[text()='Price']/following-sibling::*//input")).getAttribute("value");
//        return getText(By.cssSelector(".bid-box"));

    }

    private String getDefaultOfferPriceVirtual() {
        return find(By.xpath("//*[text()='Price']/following-sibling::*//input")).getAttribute("value");
//        return getText(By.cssSelector(".trade-last-price b"));

    }

//    private String getDefaultOfferPriceReal() {
//        return find(By.xpath(""))
//    }

    private String getDefaultOfferPriceToSell() {
        return find(By.xpath("//div[@class='sellbuycontent']//child::div[5]/div[2]/div//input")).getAttribute("value");
    }

    public void inputPriceUpperReject() {
        String price = getDefaultOfferPriceVirtual();
        System.out.println(price);

        Integer newPriceDouble = Integer.parseInt(price) * 2;
        System.out.println(newPriceDouble);

        inputPrice(String.valueOf(newPriceDouble));
    }

//    public void inputPriceUpperRejectReal() {
//        String price = getDefaultOfferPrice();
//        System.out.println(price);
//
//        double newPriceDouble = Double.parseDouble(price) * 2;
//        System.out.println(newPriceDouble);
//
//        inputPriceReal(String.valueOf(newPriceDouble));}

//    public void inputPriceUpperReject() {
////        String price = getDefaultOfferPrice();
////        String price = getText(By.xpath("//*[text()='Price']/following-sibling::*//div/div[2]"));
//        String price = getText(By.cssSelector(".trade-last-price b"));
////        System.out.println(price);
//        Double newPriceDouble = Double.parseDouble(price);
////        System.out.println(newPriceDouble);
//
//
////        int price = 500000000;
////        double newPriceDouble = price * 2;
//        inputPrice(String.valueOf(newPriceDouble * 2));
//    }

    public void inputPriceUpperRejectReal() {
        String price = getDefaultOfferPrice();
        System.out.println(price);

        double newPriceDouble = Double.parseDouble(price) * 2;
        System.out.println(newPriceDouble);


//        int price = 500000000;
//        double newPriceDouble = price * 2;
        inputPriceReal(String.valueOf(newPriceDouble));
    }

    public void inputPriceBelowReject() {
//        String price = getDefaultOfferPrice();
//        double newPriceDouble = Double.parseDouble(price) / 2;

        int price = 5;
        double newPriceDouble = price;
//        if (newPriceDouble < 50) newPriceDouble = 50;
        inputPrice(String.valueOf(newPriceDouble));
    }

    public void inputPriceBelowRejectReal() {
//        String price = getDefaultOfferPrice();
//        double newPriceDouble = Double.parseDouble(price) / 2;

        int price = 5;
        double newPriceDouble = price;
//        if (newPriceDouble < 50) newPriceDouble = 50;
        inputPriceReal(String.valueOf(newPriceDouble));
    }

    public void inputPriceBelowAsk(double amount) {
//        String price = getText(By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div//input"));
//       String price= getAttribute(By.xpath(getPriceXpath), "value");
       String price = getDefaultOfferPrice();
       System.out.println(price);
//        String price = getText(By.cssSelector(".ordrbook-item-list td:nth-child(3)"));
//        if (price.equals("-")) price = getDefaultOfferPrice();

        double newPriceDouble = Double.parseDouble(price) - amount;
        System.out.println(newPriceDouble);


        inputPrice(String.valueOf(newPriceDouble));
    }

    public void inputPriceRealBelowAsk(double amount) {
//        String price = getText(By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div//input"));
//       String price= getAttribute(By.xpath(getPriceXpath), "value");
        String price = getDefaultOfferPrice();
        System.out.println(price);
//        String price = getText(By.cssSelector(".ordrbook-item-list td:nth-child(3)"));
//        if (price.equals("-")) price = getDefaultOfferPrice();

        double newPriceDouble = Double.parseDouble(price) - amount;
        System.out.println(newPriceDouble);


        inputPriceReal(String.valueOf(newPriceDouble));
    }


//    String price = getDefaultOfferPrice();
//        System.out.println(price);
//
//    double newPriceDouble = Double.parseDouble(price) * 2;
//        System.out.println(newPriceDouble);
//
//
//    //        int price = 500000000;
////        double newPriceDouble = price * 2;
//    inputPrice(String.valueOf(newPriceDouble));

    public void clickAddPrice() {
        clickOn(By.xpath("//*[text()='Price']/following-sibling::*" +
                "//span[contains(@class, 'ant-input-number-handler-up') and @role='button']"));
    }

    public void clickSubtractPrice() {
        clickOn(By.xpath("//*[text()='Price']/following-sibling::*" +
                "//span[contains(@class, 'ant-input-number-handler-down') and @role='button']"));
    }

    public String getInputPrice() {
        return getAttribute(By.xpath(inputPriceXpath), "value")
                .replaceAll("[,;\\\\s]", "");
    }

    public void clickSell() {
        clickOn(By.cssSelector("li.ant-dropdown-menu-item:nth-child(2)"));
    }

    public void inputPriceAboveBid(double amount) {
//        String price = getText(By.cssSelector(".ordrbook-item-list td:nth-child(3)"));
//        String price= getAttribute(By.xpath(getPriceXpath), "value");
//        if (price.equals("-")) price = getDefaultOfferPrice();

        String price = getDefaultOfferPrice();
        System.out.println(price);

        double newPriceDouble = Double.parseDouble(price) + amount;
        System.out.println(newPriceDouble);


        inputPrice(String.valueOf(newPriceDouble));

//        int newPriceInt = Integer.parseInt(price) + amount;
//        inputPrice(String.valueOf(newPriceInt));
    }

    public void inputPriceAboveBidReal(double amount) {
//        String price = getText(By.cssSelector(".ordrbook-item-list td:nth-child(3)"));
//        String price= getAttribute(By.xpath(getPriceXpath), "value");
//        if (price.equals("-")) price = getDefaultOfferPrice();

        String price = getDefaultOfferPrice();
        System.out.println(price);

        double newPriceDouble = Double.parseDouble(price) + amount;
        System.out.println(newPriceDouble);


        inputPriceReal(String.valueOf(newPriceDouble));

//        int newPriceInt = Integer.parseInt(price) + amount;
//        inputPrice(String.valueOf(newPriceInt));
    }

    public void inputPriceAboveBidToSell(double amount){
        String price = getDefaultOfferPriceToSell();
        System.out.println(price);

        double newPriceDouble = Double.parseDouble(price) + amount;
        System.out.println(newPriceDouble);


        inputPriceToSell(String.valueOf(newPriceDouble));
    }

    public void clickRealTrading() {
        clickOn(By.cssSelector(".login-real a"));
    }

    public void inputSinarmasSekuritasUsername(String username) {
        typeOn(By.cssSelector("input[name='tradeUsername']"), username);
    }

    public void inputSinarmasSekuritasPassword(String password) {
        typeOn(By.cssSelector("input[name='tradePassword']"), password);
    }

    public void clickSinarmasSekuritasLogin() {
        clickOn(By.cssSelector("button.tombol-login"));
    }

    public void inputSinarmasSekuritasPin(String pin) {
        typeOn(By.cssSelector("input[name='tradingPin']"), pin);
    }

    public void clickConfirmSinarmasSekuritasPin() {
        clickOn(By.cssSelector("button.button-pin"));
    }

    public void clickLanjutSekuritas() {
        clickOn(By.cssSelector("button.tradepin-modal-button"));
    }

    public String getNotifInfo() {
        return getText(By.xpath("//body/div[@id='app-react']/div[@id='main-wrapper']/div[3]"));
    }

    public void addOrderLotInForm() { clickOn(By.xpath("//div[@class='sell-buy-form']//child::div[4]/div[2]/div[1]/div/span[1]"));}

    public void clickMahakaryaSekuritasLogin() {
        clickOn(By.cssSelector(".login-btn"));
    }

    public void inputMahakaryaSekuritasPin(String pin) {
        typeOn(By.cssSelector(".securities-input-pin"), pin);
    }

    public void clickConfirmMahakaryaSekuritasPin() {
        clickOn(By.cssSelector(".login-btn"));
    }

    public void clickVirtualTrading() {
        clickOn(By.cssSelector(".platform-item"));
    }
}
