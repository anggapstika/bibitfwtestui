package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class EarningsPage extends BasePageObject {
    public void selectSector() {
        clickOn(By.cssSelector(".ant-select-selection__rendered"));
        clickOn(By.cssSelector(".ant-select-tree li"));
    }

    public void inputSymbol(String symbol) {
        typeOn(By.name("symbol"), symbol);
    }

    public void clickSearch() {
        clickOn(By.cssSelector(".earning-symbol-search"));
    }

    public String getSearch() {
        return getText(By.cssSelector(".symbol a"));
    }
}
