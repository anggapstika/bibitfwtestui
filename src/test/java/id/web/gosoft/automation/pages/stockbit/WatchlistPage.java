package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class WatchlistPage extends BasePageObject {
    public void removeSymbolFromWatchlist(String symbol) {
        if (isPresent(By.cssSelector("a[href='#/symbol/" + symbol + "']"))) {
            By by = By.xpath("//*[@class='ant-table-fixed-left']//a[@href='#/symbol/"
                    + symbol
                    + "']/following-sibling::*/following-sibling::*");

            mouseOver(by);
            clickOn(by);
            clickOn(By.cssSelector("input[value='Delete']"));
        }
    }

    public void searchSymbolWatchlist(String symbol) {
        typeOn(By.className("wladdsymbol"), symbol);
    }

    public void clickFirstSymbolResult() {
        clickOn(By.cssSelector(".option.stock"));
    }

    public boolean isSymbolPresentOnWatchlist(String symbol) {
        return isPresent(By.cssSelector("a[href='#/symbol/" + symbol + "']"));
    }

    public void clickAddFinancialColumn() {
        clickOn(By.id("wladdcolumn"));
    }

    public void searchColumn(String query) {
        typeOn(By.cssSelector("input.metric-search"), query);
        waitABit(2);
        clickOn(By.id("react-autowhatever-metric-search--item-0"));
    }

    public String getFirstNewColumn() {
        return getText(By.className("dragtable-drag-handle"));
    }

    public void removeColumn() {
        By by = By.cssSelector("a.wl-delete-item.delete-column");
        mouseOver(by);
        clickOn(by);
    }

    public void removeColumnIfExists() {
        By by = By.cssSelector("a.wl-delete-item.delete-column");
        if (isPresent(by)) {
            mouseOver(by);
            clickOn(by);
        }
    }

    public boolean isFinancialColumnIsDeleted() {
        return isPresent(By.className("dragtable-drag-handle"));
    }

    public void openWatchlistDropdown() {
        clickOn(By.cssSelector(".wlbutton.floating-button-single"));
    }

    public void clickCreateWatchlistMenu() {
        clickOn(By.className("newwl"));
    }

    public void inputWatchlistName(String watchlist) {
        typeOn(By.className("newwl-input"), watchlist);
    }

    public void clickCreateWatchlist() {
        clickOn(By.className("newwl-save"));
    }

    public String getCurrentWatchlistName() {
        return getText(By.cssSelector(".wlbutton.floating-button-single"));
    }

    public void scrollToBottomOfWatchlist() {
        scrollToBottomOfDiv(By.className("watchlist-page-popover-item"));
    }

    public void clickEditWatchList(String name) {
        String xpath = "//span[text()='" + name + "']/following-sibling::i";
        clickOn(By.xpath(xpath));
    }

    public void inputEditWatchlist(String name) {
        typeOn(By.cssSelector("input.crtnewwl-name"), name);
    }

    public void clickConfirmEdit() {
        clickOn(By.cssSelector(".mdl-btn.mdl-action"));
    }

    public void clickDeleteWatchlist(String name) {
        String xpath = "//span[text()='" + name + "']/following-sibling::i/following-sibling::i";
        clickOn(By.xpath(xpath));
        clickOn(By.cssSelector(".modal-button.modal-save"));
    }

    public boolean isWatchlistPresent(String name) {
        return isPresent(By.xpath("//span[text()='" + name + "']"));
    }
}
