package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import id.web.gosoft.automation.webdriver.WebdriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//class LandingPage
public class LandingPage extends BasePageObject {
    public boolean isOnPage() {
        WebDriverWait wait = new WebDriverWait(getDriver(), 15);
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.id("landing-wrapper")))
                .isDisplayed();
    }

    public void clickMenuLogin() {
        clickOn(By.className("login-ldn"));
    }


}
