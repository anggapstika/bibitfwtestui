package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

//class LandingPage
public class ChatPage extends BasePageObject {
    public void clickNewChatMenu() {
        clickOn(By.cssSelector("span.chatnew"));
    }

    public void inputRecipient(String username) {
        typeOn(By.cssSelector("input[name='username']"), username);
        waitABit(8);
    }

    public void clickStartChat() {
//        clickOn(By.cssSelector("button.mdl-action"));
        clickOn(By.cssSelector(".people_revamp"));
    }

    public void inputMessage(String message) {
        typeOn(By.cssSelector("#chatv2textarea"), message);
    }

    public void clickSendChat() {
        clickOn(By.cssSelector(".chatbutton.send"));
    }

    public String getMyRecentMessage() {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        return getText(By.cssSelector(".message.chat:last-of-type .chat-content > div"));
    }

    public String getMyRecentShareMessage() {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        return getText(By.cssSelector(".chat-preview-textmedia"));
    }

    public boolean isSharePostInMyNewestMessage(String share) {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        String selector = ".message.chat:last-of-type .chat-content > div > img[title=':" + share + ":']";
        return isPresent(By.cssSelector(selector));
    }

    public void clickChooseEmoji() {
        clickOn(By.cssSelector(".chatbutton.happy"));
    }

    public void searchEmoji(String emoji) {
        typeOn(By.cssSelector("input.emoji-search"), emoji);
        clickOn(By.cssSelector(".ReactVirtualized__Grid .emoji"));
    }

    public boolean isEmojiExistsInMyNewestMessage(String emoji) {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        String selector = ".message.chat:last-of-type .chat-content > div > img[title=':" + emoji + ":']";
        return isPresent(By.cssSelector(selector));
    }

    public void clickAttachFile() {
        clickOn(By.cssSelector(".chatbutton.attach"));
    }

    public void selectImage() {
        // Path for BrowserStack
        String key = "C:\\Users\\hello\\Documents\\images\\wallpaper1.jpg";

        // Local path
//        String key = "C:\\Users\\Bryan\\Pictures\\image.jpg";
        find(By.cssSelector("input.compose-image-hidden[type='file']")).sendKeys(key);
    }

    public boolean isImageExistsInMyNewestMessage() {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        String selector = ".message.chat:last-of-type .bubble-right .overlay-img";
        return isPresent(By.cssSelector(selector));
    }

    public void clickChooseGif() {
        clickOn(By.cssSelector(".chatbutton.gif"));
    }

    public void searchGif(String gif) {
        typeOn(By.cssSelector("input.stream-gif-search"), gif);
        clickOn(By.cssSelector(".modal-gif-item"));
    }

    public boolean isGifExistsInMyNewestMessage() {
        scrollToBottomOfDiv(By.cssSelector(".ref-chat-wrapper.chat-wrapper"));
        String selector = ".message.chat:last-of-type .bubble-right .overlay-img img";
        WebElement element = find(By.cssSelector(selector));
        String src = element.getAttribute("src");
        return src.contains(".gif");
    }

    public void clickChatMore() {
        clickOn(By.className("chat-title-more"));
    }

    public void clickClearChat() {
        clickOn(By.cssSelector(".chat-title-more-menu span:first-child"));
    }

    public void clickDeleteChat() {
        clickOn(By.cssSelector(".chat-title-more-menu span:last-child"));
    }

    public void clickConfirmClearOrDelete() {
        clickOn(By.cssSelector("button.ant-btn-primary"));
    }

    public boolean isChatEmpty() {
        return isPresent(By.cssSelector(".chat-empty.chat-wrapper"));
    }

    public boolean isUserInRecentChats(String username) {
        String xpath = "//*[@class='chatcontact-username' and text()='" + username + "']";
        return isPresent(By.xpath(xpath));
    }

    public void inputSearchContact(String user) {
        typeOn(By.cssSelector(".chatcontact-search input"), user);
    }

    public void clickFirstChatRow() {
        clickOn(By.cssSelector(".chatcontact-row"));
    }
}
