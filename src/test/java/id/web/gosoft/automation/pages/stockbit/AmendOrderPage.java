package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class AmendOrderPage extends BasePageObject{


    public boolean isOnPortofolioPage(){
        return isPresent(By.cssSelector(".trading-portfolio"));
    }

    public void clickOrder(){
        clickOn(By.xpath("//a[contains(text(),'Order')]"));
    }

    public void clickAmend(){
        clickOn(By.cssSelector(".action-amend"));
    }

    public String userGetOderStatus(){
        return getText(By.cssSelector("//tbody[@class='ant-table-tbody']//child::tr[2]/td[2]/span"));
    }

    public void clickBuy(){
        clickOn(By.cssSelector(".trade"));
    }

    public void clickAddOrder(){
        clickOn(By.xpath("//div[@class='sell-buy-form']//child::div[4]/div[2]/div[1]/div[1]/span[1]"));
    }

    public void clickLessOrderPrice(){
        clickOn(By.xpath("//div[@class='sell-buy-form']//child::div[3]/div[2]/div[1]/div[1]/span[2]"));
    }

}
