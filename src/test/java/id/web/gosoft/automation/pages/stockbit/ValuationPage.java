package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class ValuationPage extends BasePageObject {
    public void selectValuationMethod(String method) {
        select(By.cssSelector("select[name='method']"))
                .selectByVisibleText(method);
    }

    public void inputQuerySymbol(String symbol) {
        typeOn(By.cssSelector("input[name='symbol']"), symbol);
    }

    public void clickFirstSearchResult() {
        clickOn(By.cssSelector("li[role='option']:first-child"));
    }

    public void selectValuationGrowth(String growth) {
        select(By.cssSelector("select[data-metric='GROWTH']"))
                .selectByVisibleText(growth);
    }

    public void selectValuationMultiple(String multiple) {
        select(By.cssSelector("select[data-metric='MULTIPLE']"))
                .selectByVisibleText(multiple);
    }

    public void clickValueBtn() {
        clickOn(By.cssSelector("button[type='submit']"));
    }

    public boolean isValuationResultPresent() {
        return isPresent(By.id("valuation-container"));
    }

    public void inputValuationNote(String note) {
        By by = By.cssSelector("textarea[name='note']");
        getDriver().findElement(by).clear();
        typeOn(by, note);
    }

    public void clickSaveValuation() {
        clickOn(By.cssSelector("#valuation-container button[type='submit']"));
    }

    public void inputValuationName(String name) {
        typeOn(By.id("val_name"), name);
    }

    public void clickConfirmCreate() {
        clickOn(By.cssSelector("button.ant-btn-primary"));
    }

    public String getValuationName() {
        return getText(By.cssSelector(".general-title span"));
    }

    public String getValuationNote() {
        return getText(By.cssSelector("textarea[name='note']"));
    }

    public String getSymbolWarning() {
        return getDriver().findElement(By.cssSelector("input[name='symbol']")).getAttribute("validationMessage");
    }

    public void clickValuationMenuBtn() {
        clickOn(By.cssSelector(".wl-more.wlbutton"));
    }

    public void clickDeleteValuationFromMenu() {
        clickOn(By.cssSelector(".wl-pagemenu-delete"));
    }

    public void clickConfirmDelete() {
        clickOn(By.xpath("//span[text()='Delete']/parent::button"));
    }

    public String getSuccessAlertMessage() {
        return getText(By.cssSelector(".sysmsg.alert-top.alert-green"));
    }

    public void openListValuationMenu() {
        clickOn(By.cssSelector(".general-title span"));
    }

    public boolean isValuationExist(String name) {
        String xpath = "//*[contains(@class,'wl-boundary')]//i[text()='" + name + "']";
        return isPresent(By.xpath(xpath));
    }

    public void clickEditValuationFromMenu() {
        clickOn(By.cssSelector(".wl-pagemenu-edit"));
    }

    public void inputEditValuationName(String name) {
        typeOn(By.cssSelector("input[placeholder='Valuation Name']"), name);
    }

    public void clickConfirmUpdate() {
        clickOn(By.xpath("//span[text()='Update']/parent::button"));
    }

}
