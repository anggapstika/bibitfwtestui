package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class SearchPage extends BasePageObject {
    public void inputSearchQuery(String query) {
        typeOn(By.cssSelector("input.searchbox"), query);
    }

    public void inputSearchProfile(String query) {
        typeOn(By.cssSelector("input.searchbox"), query);
        waitABit(3);
        find(By.cssSelector("input.searchbox")).sendKeys(Keys.ENTER);
    }

    public void clickFirstSearchResult() {
//        clickOn(By.id("react-autowhatever-search_global-section-0-item-0"));
        clickOn(By.cssSelector("#react-autowhatever-search_global-section-0-item-0"));
//        clickOn(By.cssSelector("#react-autowhatever-search_global-section-0-item-0"));
//        clickOn(By.cssSelector(".react-autosuggest__suggestions-list"));

    }

    public String getCompanySearchResult() {
          return getText(By.xpath("//div[@id='react-autowhatever-search_global']//child::div[1]/ul[1]/li[1]/div/b"));
    }

    public String getCompanyName() {
        return getText(By.cssSelector(".company-name>span"));
    }

    public String getUserSearchResult() {
//        return getText(By.cssSelector(".option.people>b"));
        return getText(By.cssSelector(".recent_search_people_fullname"));
    }

    public String getProfileName() {
//        return getText(By.cssSelector(".main-profile-text>h1"));
        return getText(By.cssSelector(".main-profile-friend-discovery>div>h1"));
    }

    public String getCatalogSearchResult() {
        return getText(By.cssSelector(".catalog-item"));
    }

    public String getCatalogName() {
        return getText(By.cssSelector(".basket-head>h1"));
    }

    public String getInsiderSearchResult() {
        return getText(By.cssSelector(".option.insider>b"));
    }

    public void clickFirstInsiderResult() {
        clickOn(By.cssSelector(".option.insider"));
    }

    public String getInsiderName() {
        return getText(By.cssSelector(".insiderfeature h2"));
    }

    public boolean isNoResultDisplayed() {
        return isPresent(By.cssSelector(".sr-people"));
    }

    public String getNotification() {
        return getText(By.xpath("//div[contains(text(),'No Results for People')]"));
    }

    public boolean isVerifiedBadgeDisplayedOnSearch() {
        return isDisplayed(By.cssSelector(".icon-new-verified-profile"));
    }

}
