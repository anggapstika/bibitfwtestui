package id.web.gosoft.automation.pages.google;

import id.web.gosoft.automation.pages.base.BasePageObject;
import id.web.gosoft.automation.webdriver.WebdriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchResultPage extends BasePageObject {

  public void clickFirstSearchResult() {
    WebElement firstResult = getDriver()
        .findElement(By.xpath("(//h3)[1]"));
    firstResult.click();
  }

}
