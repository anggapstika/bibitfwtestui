package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

import java.util.Set;

//class LoginPage
public class LoginPage extends BasePageObject {
    String parentWindow = getDriver().getWindowHandle();

    public void inputUsername(String username) {
        typeOn(By.id("username"), username);
    }

    public void inputPassword(String password) {
        typeOn(By.id("password"), password);
    }

    public void clickBtnLogin() {
        clickOn(By.id("loginbutton"));
    }

    public String getUsernameWarning() {
        return getDriver().findElement(By.id("username")).getAttribute("validationMessage");
    }

    public String getPasswordWarning() {
        return getDriver().findElement(By.id("password")).getAttribute("validationMessage");
    }

    public boolean isBtnLoginDisplayed() {
        return isPresent(By.id("loginbutton"));
    }

    public void clickLoginWithGoogleButton() {
        clickOn(By.cssSelector("input.login-goog"));
    }

    public void clickLoginWithFacebookButton() {
        clickOn(By.cssSelector("a.login-fb"));
    }

    public void switchToNewlyOpenedWindow() {
        Set<String> windows = getDriver().getWindowHandles();
        for (String window : windows)
            getDriver().switchTo().window(window);
    }

    public void inputFacebookEmail(String email) {
        typeOn(By.id("email"), email);
    }

    public void inputFacebookPassword(String password) {
        typeOn(By.id("pass"), password);
    }

    public void clickFacebookLoginButton() {
        clickOn(By.cssSelector("input[value='Log In']"));
    }

    public void inputGoogleEmail(String email) {
        typeOn(By.id("identifierId"), email);
    }

    public void clickGoogleNextButton() {
        clickOn(By.xpath("//span[text()='Next' or text()='Berikutnya']//parent::button"));
    }

    public void inputGooglePassword(String password) {
        typeOn(By.cssSelector("input[type='password']"), password);
    }

    public void switchToParentWindow() {
        getDriver().switchTo().window(parentWindow);
    }

    public void userCloseAvatarPopup(){
        //if (isPresent(By.xpath("//div[@class='rateUsWrap']"))){
        //    clickOn(By.xpath("//div[@class='rateUs']//div[@class='RateUsClose']"));
        //}

        try {
            if (isPresent(By.cssSelector(".ant-modal-content"))) {
                clickOn(By.cssSelector(".skip-btn"));

            }
        }
        catch (Exception e) {
            return;
        }
    }


}
