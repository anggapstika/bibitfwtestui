package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;

public class CalendarPage extends BasePageObject {
    public void clickSubmenu(String submenu) {
        By by = By.xpath("//a[contains(text(),'" + submenu + "')]");
        if (!isClickable(by)) {
            by = By.xpath("//div[contains(@class, 'menu-more')]//a[contains(text(),'" + submenu + "')]");
            clickOn(By.cssSelector(".menu.calendar-more"));
        }
        clickOn(by);
    }

    public boolean isEconomicTableDisplayed() {
        return isPresent(By.cssSelector(".cal-economic"));
    }

    public String getFirstSymbol() {
        return getText(By.cssSelector("td a"));
    }

    public void clickSymbol() {
        clickOn(By.cssSelector("td a"));
    }

    public boolean isIpoTableDisplayed() {
        return isPresent(By.cssSelector(".cal-ipo"));
    }

    public void inputDateForm(String datefrom) {
        clickOn(By.xpath("//input[@id='dp1613546798056']"));
        typeOn(By.xpath("//input[@id='dp1613546798056']"), datefrom);
    }

    public void inputDateTo(String dateto) {
        clickOn(By.xpath("//input[@id='dp1613546798055']"));
        typeOn(By.xpath("//input[@id='dp1613546798055']"), dateto);
    }

    public void clickSearch() {
        clickOn(By.xpath("//*[@id=\"today_event\"]/div[2]/button"));
    }
}

