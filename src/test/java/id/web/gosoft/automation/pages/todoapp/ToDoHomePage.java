package id.web.gosoft.automation.pages.todoapp;

import id.web.gosoft.automation.webdriver.AndroidDriverInstance;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ToDoHomePage {

  public void clickButtonAdd() {
    AndroidElement buttonAdd = (AndroidElement) AndroidDriverInstance.webDriver
        .findElement(By.id("fab_add_task"));
    buttonAdd.click();
  }

  public void checkUncheckTaskName(String taskName, String key) {
    if (key.equals("check") && !getCheckedStatus(taskName)) {
      getCheckElement(taskName).click();
    } else if (key.equals("uncheck") && getCheckedStatus(taskName)) {
      getCheckElement(taskName).click();
    }
  }

  public WebElement getCheckElement(String taskName) {
    Integer order = getTaskOrder(taskName);
    return (WebElement) AndroidDriverInstance.webDriver.findElements(By.id("complete")).get(order);
  }

  public boolean checkTaskShown(String taskName) {
    Integer order = getTaskOrder(taskName);
    return order != null;
  }

  public Integer getTaskOrder(String taskName) {
    List<WebElement> tasks = AndroidDriverInstance.webDriver
        .findElements(By.id("title"));
    for (int i = 0; i < tasks.size(); i++) {
      if (taskName.equalsIgnoreCase(tasks.get(i).getText())) {
        return i;
      }
    }
    return null;
  }

  public boolean getCheckedStatus(String taskName) {
    WebElement checked = getCheckElement(taskName);
    return Boolean.parseBoolean(checked.getAttribute("checked"));
  }

  public void clickHamburgerButton() {
    AndroidDriverInstance.webDriver
        .findElement(By.xpath("//android.widget.ImageButton[@content-desc='Navigate up']")).click();
  }

  public void clickFilterButton() {
    AndroidDriverInstance.webDriver.findElement(By.id("menu_filter")).click();
    waitForAnimation();
  }

  public void clickMoreOptionsButton() {
    AndroidDriverInstance.webDriver
        .findElement(By.xpath("//android.widget.ImageView[@content-desc='More options']")).click();
    waitForAnimation();
  }

  public void selectFilter(String filterName) {
    selectDropdownMenu("title", "Active");
  }

  public void selectMoreOptionsMenu(String menuName) {
    selectDropdownMenu("title", menuName);
  }

  public void selectMenu(String menu) {
    selectDropdownMenu("design_menu_item_text", menu);
  }

  public void selectDropdownMenu(String locator, String name) {
    List<WebElement> filters = AndroidDriverInstance.webDriver.findElements(By.id(locator));
    for (WebElement filter : filters) {
      if (filter.getText().equalsIgnoreCase(name)) {
        filter.click();
        break;
      }
    }
    waitForAnimation();
  }

  public void waitForAnimation() {
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public boolean isOnPage() {
    WebDriverWait wait = new WebDriverWait(AndroidDriverInstance.webDriver, 15);
    return wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fab_add_task")))
        .isDisplayed();
  }

  public String getListTaskName() {
    return AndroidDriverInstance.webDriver.findElement(By.id("filteringLabel")).getText();
  }

}
