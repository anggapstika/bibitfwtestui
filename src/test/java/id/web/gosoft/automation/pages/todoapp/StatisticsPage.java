package id.web.gosoft.automation.pages.todoapp;

import id.web.gosoft.automation.webdriver.AndroidDriverInstance;
import org.openqa.selenium.By;

public class StatisticsPage {

  public String getStatisticData() {
    return AndroidDriverInstance.webDriver.findElement(By.id("statistics")).getText();
  }

}
