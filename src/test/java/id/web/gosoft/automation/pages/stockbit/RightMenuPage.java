package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;


public class RightMenuPage extends BasePageObject {

    public void clickPriceAlertRightMenu(){
        clickOn(By.xpath("//*[@id=\"rightmenu\"]/a[2]/i"));
    }

    public void clickNewAlert(){
        clickOn(By.xpath("//div[@class='widgetboxin ']//child::span[@class='add-pricealert']/i"));
    }

    public void inputStocksName(String stck){
        typeOn(By.name("symbol"), stck);
    }

    public void selectCondition(String condition) {
        select(By.cssSelector(".pa-condition select"))
                .selectByVisibleText(condition);
    }

    public void inputValue(String val){
        typeOn(By.cssSelector(".pa-value input"),val);
    }

    public void inputDesc(String desc){
        typeOn(By.cssSelector(".pa-description textarea"),desc);
    }

    public void clickButtonCreate(){
        clickOn(By.cssSelector(".ant-btn-primary"));
    }

    public void displayedPriceAlert(){
        isDisplayed(By.xpath("//*[@id=\"wgtalert\"]/div/div/div[1]/div[1]/div[2]/div[1]/div/div/div/div/div/table/tbody"));
    }

    public void clickDeleteAlert(){
        clickOn(By.xpath("//*[@id=\"wgtalert\"]/div/div/div[1]/div[1]/div[2]/div[1]/div/div/div/div/div/table/tbody/tr/td[3]/span/a[2]/i"));
    }

    public void clickDeleteYesButton(){
//        clickOn(By.xpath("/html/body/div[7]/div/div[2]/div/div[1]/div[3]/button[2]"));
        clickOn(By.xpath("//button[@class='ant-btn ant-btn-primary ant-btn-lg']"));
    }

    public void clickRightMenuHotlist(){
        clickOn(By.xpath("//*[@id=\"rightmenu\"]/a[4]/i"));
    }

    public String getHotlistSection(){
        return getText(By.xpath("//*[@id=\"wgthotlist\"]/div/div/div[1]/div[1]/div/div[2]/div[1]/h3"));
    }

    public void clickHotlistSymbol(){
        clickOn(By.xpath("//*[@id=\"wgthotlist\"]/div/div/div[1]/div[1]/div/div[3]/div[1]/div[1]"));
    }

    public void srollingRightPanel(){
        //scrollByPixels(By.cssSelector(".ui-resizable-handle.ui-resizable-s"),0,510);
        scrollIntoView(By.xpath("//*[@id=\"valuation-result\"]/div[7]/div/button"));
    }

    public void clickValuatioanRightMenu(){
        clickOn(By.xpath("//*[@id=\"rightmenu\"]/a[5]/i"));
    }

    public void clickValueButtonRightMenu(){
        clickOn(By.xpath("//*[@id=\"valuationdiv\"]/div[1]/form/div/button"));
    }

    public void selectValuationMethod(String method) {
        select(By.cssSelector("select[name='method']"))
                .selectByVisibleText(method);
    }

    public void inputQuerySymbol(String symbol) {
        typeOn(By.cssSelector("input[name='symbol']"), symbol);
    }

    public void clickFirstSearchResult() {
        clickOn(By.cssSelector("li[role='option']:first-child"));
    }

    public void selectValuationGrowth(String growth) {
        select(By.cssSelector("select[data-metric='GROWTH']"))
                .selectByVisibleText(growth);
    }

    public void selectValuationMultiple(String multiple) {
        select(By.cssSelector("select[data-metric='MULTIPLE']"))
                .selectByVisibleText(multiple);
    }

    public boolean isValuationResultPresent() {
        return isPresent(By.id("valuation-container"));
    }

    public void inputValuationNote(String note) {
        typeOn(By.cssSelector("textarea[name='note']"), note);
    }

    public void clickSaveValuation() {
        clickOn(By.cssSelector("#valuation-container button[type='submit']"));
    }

    public void inputValuationName(String name) {
        typeOn(By.id("val_name"), name);
    }

    public void clickConfirmCreate() {
        clickOn(By.cssSelector("button.ant-btn-primary"));
    }

    public String getValuationName() {
        return getText(By.cssSelector(".general-title span"));
    }

    public String getValuationNote() {
        return getText(By.cssSelector("textarea[name='note']"));
    }

    public String getSymbolWarning() {
        return getDriver().findElement(By.cssSelector("input[name='symbol']")).getAttribute("validationMessage");
    }

    public void clickValuationMenuBtn() {
        clickOn(By.cssSelector(".wl-more.wlbutton"));
    }

    public void clickDeleteValuationFromMenu() {
        clickOn(By.cssSelector(".wl-pagemenu-delete"));
    }

    public void clickConfirmDelete() {
        clickOn(By.xpath("//span[text()='Delete']/parent::button"));
    }

    public String getSuccessAlertMessage() {
        return getText(By.cssSelector(".sysmsg.alert-top.alert-green"));
    }

    public void openListValuationMenu() {
        clickOn(By.cssSelector(".general-title span"));
    }

    public boolean isValuationExist(String name) {
        String xpath = "//*[contains(@class,'wl-boundary')]//i[text()='" + name + "']";
        return isPresent(By.xpath(xpath));
    }

    public void clickEditValuationFromMenu() {
        clickOn(By.cssSelector(".wl-pagemenu-edit"));
    }

    public void inputEditValuationName(String name) {
        typeOn(By.cssSelector("input[placeholder='Valuation Name']"), name);
    }

    public void clickConfirmUpdate() {
        clickOn(By.xpath("//span[text()='Update']/parent::button"));
    }

    public void clickBandarDetector() {clickOn(By.xpath("//*[@id=\"rightmenu\"]/a[6]/i"));}

    public void inputCompanyBandDetect(String cmpbnd){
        typeOn(By.cssSelector("input[name='symbol']"),cmpbnd);
    }

    public void clickFirstSearch() {
        clickOn(By.cssSelector("li[role='option']:first-child"));
    }

    public void clickCalendarIcon(){
        clickOn(By.xpath("//*[@id=\"wgtbandar\"]/div/div[2]/form/div/a"));
    }

}
