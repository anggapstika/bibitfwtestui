package id.web.gosoft.automation.pages.stockbit;

import id.web.gosoft.automation.pages.base.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class FinancialsPage extends BasePageObject {
    public void selectReportType(String report) {
        select(By.name("reporttype"))
                .selectByVisibleText(report);
    }

    public void selectStatementType(String statement) {
        select(By.name("statementtype"))
                .selectByVisibleText(statement);
    }

    public void selectCurrency(String currency) {
        select(By.name("currency"))
                .selectByVisibleText(currency);
    }

    public void selectOrdertype(String ordertype) {
        select(By.name("ordertype"))
                .selectByVisibleText(ordertype);
    }

    public String getTableTitle() {
        return getText(By.cssSelector("#data_table_1 span.info"));
    }

    public void clickSortTable(String order) {
        clickOn(By.cssSelector(".fmgroup button[title='" + order + "']"));
    }

    public boolean isSortedAscending() {
        List<String> list = findAll(By.cssSelector("#data_table_1 th.periods-list"))
                .stream()
                // get the year only
                .map(el -> el.getText().split(" ")[1])
                .collect(Collectors.toList());

        if (list.size() < 1) return true;

        Iterator<String> iter = list.iterator();
        String current, previous = iter.next();
        while (iter.hasNext()) {
            current = iter.next();
            if (previous.compareTo(current) > 0) return false;
            previous = current;
        }
        return true;
    }

    public boolean isSortedDescending() {
        List<String> list = findAll(By.cssSelector("#data_table_1 th.periods-list"))
                .stream()
                // get the year only
                .map(el -> el.getText().split(" ")[1])
                .collect(Collectors.toList());

        Iterator<String> iter = list.iterator();
        String current, previous = iter.next();
        while (iter.hasNext()) {
            current = iter.next();
            if (previous.compareTo(current) < 0) return false;
            previous = current;
        }
        return true;
    }

    public void clickChangeUnit(String unit) {
        clickOn(By.cssSelector(".fmgroup button[data-value='" + unit + "']"));
    }

    public boolean isValueHavePercent() {
        List<String> list = findAll(By.cssSelector("#data_table_1 .row1 td.rowval:not(.empty)"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        for (String val : list)
            if (!val.contains("%")) return false;

        return true;
    }

    public void clickRoundDown() {
        clickOn(By.cssSelector(".fmgroup button[data-opt='down']"));
    }

    public String getTableValueIdr() {
        return getAttribute(By.cssSelector("#data_table_1 .row1 td.rowval:not(.empty)"), "data-value-idr");
    }

    public String getTableTextIdr() {
        return getText(By.cssSelector("#data_table_1 .row1 td.rowval:not(.empty)"))
                .replaceAll("[^\\d.]", "");
    }

    public String getRoundedToMillions() {
        String value = getTableValueIdr();
        return value.substring(0, value.length() - 6);
    }

    public String getRoundedToThousands() {
        String value = getTableValueIdr();
        return value.substring(0, value.length() - 3);
    }

    public void clickRoundUp() {
        clickOn(By.cssSelector(".fmgroup button[data-opt='up']"));
    }

    public String getRoundedToBillions() {
        String value = getTableValueIdr();
        return value.substring(0, value.length() - 9);
    }
}
