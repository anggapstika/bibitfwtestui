package id.web.gosoft.automation.api.bibit.robo;

import id.web.gosoft.automation.api.bibit.auth.login.BaseApi;
import io.restassured.response.Response;

import static id.web.gosoft.automation.constants.EndpointConstants.ENDPOINT_CHECK_ROBO_RESULT;

public class CheckRoboResultApi extends BaseApi {
    public Response getRoboResult(String roboid) {
        Response response = baseRequestWithLogin()
                .param("roboid", roboid)
                .get(ENDPOINT_CHECK_ROBO_RESULT);

        response.getBody().prettyPrint();
        return response;
    }
}
