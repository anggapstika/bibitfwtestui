package id.web.gosoft.automation.api.bibit.auth.login;

import io.restassured.response.Response;

import static id.web.gosoft.automation.constants.EndpointConstants.ENDPOINT_GET_PORTOFOLIO_BY_CATEGORY_ID;
import static id.web.gosoft.automation.constants.EndpointConstants.ENDPOINT_GET_PORTOFOLIO_SUMMARY;

public class PortofolioApi extends BaseApi {

  public static void main(String[] args) {
    PortofolioApi api = new PortofolioApi();
    Response response = api.getPortofolio();
  }

  public Response getPortofolio() {
    Response response = baseRequestWithLogin()
        .get(ENDPOINT_GET_PORTOFOLIO_SUMMARY);
    response.getBody().prettyPrint();
    return response;
  }

  public Response getPortofolioByCategoryId(String categoryId){
    Response response = baseRequestWithLogin().pathParam("categoryId", categoryId)
        .get(ENDPOINT_GET_PORTOFOLIO_BY_CATEGORY_ID);
    response.getBody().prettyPrint();
    return response;
  }

}
