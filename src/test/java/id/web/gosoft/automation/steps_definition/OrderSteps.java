package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.CompanyPage;
import id.web.gosoft.automation.pages.stockbit.OrderPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class OrderSteps {
    OrderPage orderPage = new OrderPage();
    CompanyPage companyPage = new CompanyPage();

    @And("User click start trading")
    public void userClickStartTrading() {
        orderPage.clickStartTrading();
    }

    @And("User click virtual trading")
    public void userClickVirtualTrading() {
        orderPage.clickVirtualTrading();
    }

    @And("User click buy more")
    public void userClickBuyMore() {
        orderPage.clickPortffolioDropdown();
        orderPage.clickBuyMore();
    }

    @And("User click Add order lot")
    public void userClickAddOrderLot() {
        orderPage.clickAddLot();
    }

    @And("User click place order")
    public void userClickPlaceOrder() {
        orderPage.clickPlaceOrder();
    }

    @And("User click first trending stock")
    public void userClickFirstTrendingStock() {
        orderPage.clickFirstTrendingStock();
    }

    @And("User click buy")
    public void userClickBuy() {
        companyPage.clickBuy();
    }

    @Then("Order status is {string}")
    public void orderStatusIs(String status) {
//        orderPage.clickTradingNavIdx(TRADING_NAV_ORDER);
//        Assert.assertEquals(status, orderPage.getMostRecentStatus());
    }

    @And("User click Buy from prediction")
    public void userClickBuyFromPrediction() {
        orderPage.clickBuyFromPrediction();
    }

    @And("User click Buy from watchlist")
    public void userClickBuyFromWatchlist() {
        orderPage.clickBuyFromWatchlist();
    }

    @And("User set order lot {string}")
    public void userSetOrderLot(String lot) {
        orderPage.setOrderLot(lot);
    }

    @Then("Trading limit warning is displayed")
    public void tradingLimitWarningIsDisplayed() {
        Assert.assertTrue(orderPage.isOrderWarningPresent());
    }

    @And("User set price over upper limit")
    public void userSetPriceOverUpperLimit() {
        orderPage.inputPriceUpperReject();
    }

    @And("User set price over upper limit Real")
    public void userSetPriceOverUpperLimitReal() {
        orderPage.inputPriceUpperRejectReal();
    }

    @And("User set price below lower limit")
    public void userSetPriceBelowLowerLimit() {
        orderPage.inputPriceBelowReject();
    }

    @And("User set price below lower limit Real")
    public void userSetPriceBelowLowerLimitrReal() {
        orderPage.inputPriceBelowRejectReal();
    }

    @And("User set order price to {string}")
    public void userSetOrderPriceTo(String price) {
        orderPage.inputPrice(price);
    }

    @And("User set order price real to {string}")
    public void userSetOrderPriceRealTo(String price) {
        orderPage.inputPriceReal(price);
    }

    @And("User set order price to less {int} than lowest ask price")
    public void userSetOrderPriceToLessThanLowestAskPrice(double amount) {
        orderPage.inputPriceBelowAsk(amount);
    }

    @And("User set order price real to less {int} than lowest ask price")
    public void userSetOrderPriceRealToLessThanLowestAskPrice(double amount) {
        orderPage.inputPriceRealBelowAsk(amount);
    }


    @And("User press button plus price {int} times")
    public void userPressButtonPlusPriceAmountPlusTimes(int times) {
        for (int i = 0; i < times; i++)
            orderPage.clickAddPrice();
    }

    @And("User press button minus price {int} times")
    public void userPressButtonMinusPriceAmountMinusTimes(int times) {
        for (int i = 0; i < times; i++)
            orderPage.clickSubtractPrice();
    }

    @Then("Final set price will be {string}")
    public void finalSetPriceWillBe(String price) {
        Assert.assertEquals(price, orderPage.getInputPrice());
    }

    @And("User click sell")
    public void userClickSell() {
        orderPage.clickPortffolioDropdown();
        orderPage.clickSell();
    }

    @And("User set order price to more {int} than highest ask price")
    public void userSetOrderPriceToMoreThanHighestAskPrice(double amount) {
        orderPage.inputPriceAboveBid(amount);
    }

    @And("User set order price to more {int} than highest ask price real")
    public void userSetOrderPriceToMoreThanHighestAskPriceInReal(double amount) {
        orderPage.inputPriceAboveBidReal(amount);
    }

    @And("User click real trading")
    public void userClickRealTrading() {
        orderPage.clickRealTrading();
    }

    @And("User input sekuritas username {string}")
    public void userInputSekuritasUsername(String username) {
        orderPage.inputSinarmasSekuritasUsername(username);
    }

    @And("User input sekuritas password {string}")
    public void userInputSekuritasPassword(String password) {
        orderPage.inputSinarmasSekuritasPassword(password);
    }

    @And("User click sekuritas log in button")
    public void userClickSekuritasLogInButton() {
        orderPage.clickSinarmasSekuritasLogin();
    }

    @And("User input sekuritas pin {string}")
    public void userInputSekuritasPin(String pin) {
        orderPage.inputMahakaryaSekuritasPin(pin);
    }

    @And("User click submit pin")
    public void userClickSubmitPin() {
        orderPage.clickConfirmMahakaryaSekuritasPin();
    }

    @And("User click close sekuritas alert")
    public void userClickCloseSekuritasAlert() {
        orderPage.clickLanjutSekuritas();
    }

    @Then("User get notification {string}")
    public void userGetNotification(String notif) {
        // Assert.assertEquals(notif, orderPage.getNotifInfo());
    }

    @And("User click Add order lot in form")
    public void userClickAddOrderInForm(){
        orderPage.addOrderLotInForm();
    }

    @And("User set order price to more {int} than highest ask price to sell")
    public void userSetOrderPriceToMoreThanHighestAskPriceToSell(double amount) {
        orderPage.inputPriceAboveBidToSell(amount);
    }

    @And("User click login mahakarya sekuritas")
    public void userClickLoginMahakaryaSekuritas(){
        orderPage.clickMahakaryaSekuritasLogin();
    }
}
