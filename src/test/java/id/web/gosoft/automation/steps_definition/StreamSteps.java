package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class StreamSteps {
    MainPage mainPage = new MainPage();
    StreamPage streamPage = new StreamPage();
    CompanyPage companyPage = new CompanyPage();
    ChatPage chatPage = new ChatPage();
    ProfilePage profilePage = new ProfilePage();
    SearchPage searchPage = new SearchPage();
    String currentWord;
    String currentComment;
    String gifSrc;
    String repostWord;
    String repostUrl;
    String currentQuestion;
    String repostUrlMessage;
    String linkYoutube;

    @And("User click compose idea")
    public void userClickComposeIdea() {
        streamPage.clickCompose();
    }

    @And("User makes a unique post")
    public void userMakesAUniquePost() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        currentWord = "Just a careless prediciton, In a few years I will earn IDR " + randomNum;
        streamPage.writePost(currentWord);
    }

    @And("User click post idea")
    public void userClickPostIdea() {
        streamPage.clickPostIdea();
    }

    @Then("User post is posted")
    public void userPostIsPosted() {
        Assert.assertEquals(currentWord, streamPage.getFirstPostText());
    }

    @And("User makes the same post again")
    public void userMakesTheSamePostAgain() {
        streamPage.writePost(currentWord);
    }

    @And("User click stream menu")
    public void userClickStreamMenu() {
        mainPage.clickStreamMenu();
    }

    @And("User click profile menu")
    public void userClickProfileMenu() {
        mainPage.clickProfileMenu();
    }

    @And("User click comment icon")
    public void userClickCommentIcon() {
        streamPage.clickViewPostComment();
    }

    @And("User write a unique comment")
    public void userWriteAUniqueComment() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        currentComment = "No, I think you would earn IDR " + randomNum;
        streamPage.waitABit(3);
        streamPage.writeComment(currentComment);
    }

    @And ("User write unique question")
    public void userInputQuestion(){
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        currentQuestion = "apakah saham yang lalu akan naik?(Poll #" + randomNum + ")";
        streamPage.waitABit(3);
        streamPage.writePost(currentQuestion);
    }

    @And("User click post comment")
    public void userClickPostComment() {
        streamPage.clickPostComment();
    }

    @And("User click post comment and get error {string}")
    public void userClickPostComment(String error) {
        streamPage.clickPostComment();
        Assert.assertEquals(error, mainPage.getAlertMessage());
    }

    @Then("User comment is posted")
    public void userCommentIsPosted() {
        Assert.assertEquals(currentComment, streamPage.getLatestComment());
    }

    @And("User writes the same comment again")
    public void userWritesTheSameCommentAgain() {
        streamPage.writeComment(currentComment);
    }

    @And("User click target icon")
    public void userClickTargetIcon() {
        streamPage.clickTargetIcon();
    }

    @And("User set target symbol {string}")
    public void userSetTargetSymbol(String symbol) {
        streamPage.inputTargetSymbol(symbol);
        streamPage.clickFirstStockResult();
    }

    @And("User input target price {string}")
    public void userInputTargetPrice(String price) {
        streamPage.inputTargetPrice(price);
    }

    @And("User set target duration as {string}")
    public void userSetTargetDurationAs(String duration) {
        streamPage.selectTargetDuration(duration);
    }

    @Then("Prediction Post is posted with symbol {string} and target price {string}")
    public void predictionPostIsPostedWithSymbolAndTargetPrice(String symbol, String price) {
        mainPage.clickProfileMenu();
        Assert.assertEquals(symbol, streamPage.getIdeaTargetSymbol());
    }

    @And("User makes a symbol post with {string}")
    public void userMakesASymbolPostWith(String symbol) {
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        symbol = "$" + symbol;
        currentWord = symbol + " will recover! #" + randomNum;
        streamPage.writePost(currentWord);
    }

    @And("User clicks the symbol on most recent post")
    public void userClicksTheSymbolOnMostRecentPost() {
        streamPage.clickLatestStockIndicator();
    }

    @Then("User is redirected to {string} company page")
    public void userIsRedirectedToCompanyPage(String name) {
        Assert.assertEquals(name, companyPage.getCompanyName());
    }

    @Then("Symbol post {string} is posted without a link")
    public void symbolPostIsPostedWithoutALink(String symbol) {
        Assert.assertTrue(streamPage.isPostContainsSymbol(symbol));
        Assert.assertFalse(streamPage.isPostContainSymbolUrl());
    }

    @And("User click poll icon")
    public void userClickPollIcon() {
        streamPage.clickPollIcon();
    }

    @And("User click add choice button")
    public void userClickAddChoiceButton() {
        streamPage.clickAddPollChoice();
    }

    @And("User fill choice {int} with {string}")
    public void userFillChoiceWith(int idx, String text) {
        streamPage.inputChoiceAtIndex(idx, text);
    }

    @And("User set poll to end in {string} {string} {string}")
    public void userSetPollToEndInDaysHoursMinutes(String days, String hours, String minutes) {
        streamPage.setPollEndDate(days, hours, minutes);
    }

    @Then("Voting post is created with {string}, {string}, and {string}")
    public void votingPostIsCreatedWithAnd(String opt1, String opt2, String opt3) {
        Assert.assertEquals(opt1, streamPage.getNthPollOptionText(1));
        Assert.assertEquals(opt2, streamPage.getNthPollOptionText(2));
        Assert.assertEquals(opt3, streamPage.getNthPollOptionText(3));
    }

    @And("The poll will end in {string} {string} {string}")
    public void thePollWillEndInDaysHoursMinutes(String days, String hours, String minutes) {
        String time = "Polling ends in " + days + " days " + hours + " hours " + minutes + " minutes.";
        Assert.assertTrue(streamPage.isPollTimeLimitTheSame(time));
    }

    @And("User click gif icon")
    public void userClickGifIcon() {
        streamPage.clickGifIcon();
    }

    @And("User find gif with keyword {string}")
    public void userFindGifWithKeyword(String query) {
        streamPage.inputGifQuery(query);
    }

    @And("User pick first gif")
    public void userPickFirstGif() {
        streamPage.clickFirstGifResult();
//        gifSrc = streamPage.getFirstGifResultSrc();
    }

    @Then("Gif Post is posted")
    public void gifPostIsPosted() {
        Assert.assertTrue(streamPage.isGifPost());
//        Assert.assertEquals(gifSrc, streamPage.getPostedGifSrc());
    }

    @And("User attach file to post")
    public void userAttachFileToPost() {
        streamPage.selectFile();
    }

    @Then("File Post is posted")
    public void filePostIsPosted() {
        Assert.assertTrue(streamPage.isFilePostCreated());
    }

    @And("User attach image to post")
    public void userAttachImageToPost() {
        streamPage.selectImage();
    }

    @Then("Image Post is posted")
    public void imagePostIsPosted() {
        Assert.assertTrue(streamPage.isImagePostCreated());
    }

    @And("User clicks like on the first post")
    public void userClicksLikeOnTheFirstPost() {
        if (streamPage.isPostLiked()) streamPage.clickLikePost();
        streamPage.waitABit(3);
        streamPage.clickLikePost();
    }

    @Then("User clicks like on the first post and gets error {string}")
    public void userClicksLikeOnTheFirstPostAndGetError(String error) {
        if (streamPage.isPostLiked()) streamPage.clickLikePost();
        streamPage.waitABit(3);
        streamPage.clickLikePost();
        Assert.assertEquals(error, mainPage.getAlertMessage());
        }

    @Then("Post is liked by {string}")
    public void postIsLikedBy(String user) {
        streamPage.clickLikerList();
        Assert.assertEquals(user, streamPage.getLikerUsername());
    }

    @And("User click notification icon")
    public void userClickNotificationIcon() {
        streamPage.clickNotificationIcon();
    }

    @Then("User see all notifications")
    public void userSeeAllNotifications() {
        Assert.assertTrue(streamPage.isAllNotificationVisible());
        Assert.assertEquals(NOTIFICATION_SECTION_ALL, streamPage.getNotificationSectionName());
    }

    @When("User click notification's activity section")
    public void userClickNotificationSActivitySection() {
        streamPage.clickActivityNotifIcon();
    }

    @Then("User see activity notifications")
    public void userSeeActivityNotifications() {
        Assert.assertTrue(streamPage.isActivityNotificationVisible());
        Assert.assertEquals(NOTIFICATION_SECTION_ACTIVITY, streamPage.getNotificationSectionName());
    }

    @When("User click notification's mention section")
    public void userClickNotificationSMentionSection() {
        streamPage.clickMentionNotifIcon();
    }

    @Then("User see mention notifications")
    public void userSeeMentionNotifications() {
        Assert.assertTrue(streamPage.isMentionNotificationVisible());
        Assert.assertEquals(NOTIFICATION_SECTION_MENTION, streamPage.getNotificationSectionName());
    }

    @When("User click notification's report section")
    public void userClickNotificationSReportSection() {
        streamPage.clickReportNotifIcon();
    }

    @Then("User see report notifications")
    public void userSeeReportNotifications() {
        Assert.assertTrue(streamPage.isReportNotificationVisible());
        Assert.assertEquals(NOTIFICATION_SECTION_REPORT, streamPage.getNotificationSectionName());
    }

    @When("User click notification's tipping section")
    public void userClickNotificationSTippingSection() {
        streamPage.clickTippingNotifIcon();
    }

    @Then("User see tipping notifications")
    public void userSeeTippingNotifications() {
        Assert.assertTrue(streamPage.isTippingNotificationVisible());
        Assert.assertEquals(NOTIFICATION_SECTION_TIPPING, streamPage.getNotificationSectionName());
    }

    @And("User wait until stream is loaded")
    public void userWaitUntilStreamIsLoaded() {
        streamPage.waitUntilStreamIsLoaded();
    }

    @And("User click {string} stream section")
    public void userClickStreamSection(String section) {
        streamPage.clickStreamNav(section);
    }

    @Then("Stream will only show news post")
    public void streamWillOnlyShowNewsPost() {
        Assert.assertTrue(streamPage.isStreamNavActive(STREAM_SECTION_NEWS));
        Assert.assertTrue(streamPage.isShownNewsPost());
    }

    @Then("Stream will only show reports post")
    public void streamWillOnlyShowReportsPost() {
        Assert.assertTrue(streamPage.isStreamNavActive(STREAM_SECTION_REPORTS));
        Assert.assertTrue(streamPage.isShownReportsPost());
    }

    @Then("Stream will only show predictions post")
    public void streamWillOnlyShowPredictionsPost() {
        Assert.assertTrue(streamPage.isStreamNavActive(STREAM_SECTION_PREDICTIONS));
        Assert.assertTrue(streamPage.isShownPredictionsPost());
    }

    @Then("Stream will only show charts post")
    public void streamWillOnlyShowChartsPost() {
        Assert.assertTrue(streamPage.isStreamNavActive(STREAM_SECTION_CHARTS));
        Assert.assertTrue(streamPage.isShownChartsPost());
    }

    @Then("Stream will only show insiders post")
    public void streamWillOnlyShowInsidersPost() {
        Assert.assertTrue(streamPage.isStreamNavActive(STREAM_SECTION_INSIDERS));
        Assert.assertTrue(streamPage.isShownInsidersPost());
    }

    @When("User click share button on first post")
    public void userClickShareButtonOnFirstPost() {
        streamPage.clickShareFirstPost();
    }

    @And("User click share button on first post stream page")
    public void userClickShareButtonOnFirstPostStream(){
        streamPage.clickShareFirstPostStream();
    }

    @And("User click share button on first post polling")
    public void userClickSharePolling(){
        streamPage.clickShareFirstPolling();
    }

    @And("User click copy link button")
    public void userClickCopyLinkButton() {
        streamPage.clickCopyLink();
    }

    @Then("Post link is copied")
    public void postLinkIsCopied() {
        String firstPostId = streamPage.getFirstPostId();
        String clipboardId = streamPage.getCopiedPostId();

        Assert.assertEquals(firstPostId, clipboardId);
    }

    @And("User click repost button")
    public void userClickRepostButton() {
        streamPage.clickRepost();
        repostWord = streamPage.getRepostedText();
    }

    @Then("User repost is posted")
    public void userRepostIsPosted() {
        Assert.assertEquals(repostWord, streamPage.getFirstPostText2());
    }

    @And("User input share recipient as {string}")
    public void userInputShareRecipientAs(String user) {
        streamPage.inputMessageRecipient(user);
    }

    @And("User click more")
    public void userClickMore(){ streamPage.userClickMore(); }

    @And("User select first recipient result")
    public void userSelectFirstRecipientResult() {
        streamPage.clickFirstRecipient();
    }

    @And("User click send message")
    public void userClickSendMessage() {
        repostUrl = streamPage.getSharedMessage();
        streamPage.clickSendMessage();
    }

    @And("User search recipient {string}")
    public void userSearchRecipient(String user) {
        chatPage.inputSearchContact(user);
        chatPage.clickFirstChatRow();
    }
    @And("User scroll to main navigation")
    public void userScrollToMainNavigation() {
        streamPage.scrollToMainNavigation();
    }

    @And("User scroll to profile stream")
    public void userScrollToProfileStream() {
        streamPage.scrollToProfileStream();
    }

    @And("User scroll to stream post")
    public void userScroolToStreamPost(){
        streamPage.scroolToStreamPost();
    }

    @Then("Post is shared to recipient")
    public void postIsSharedToRecipient() {
        repostUrlMessage = streamPage.getSharedMessage2();
        String sentUrl = chatPage.getMyRecentMessage();
        Assert.assertEquals(repostUrlMessage, sentUrl);

    }

    @Then("User gets description {string}")
    public void userGetsTimestamp(String time) {
        Assert.assertEquals(time, streamPage.getTimestamp());
    }

    @And("User close share popup")
    public void userCloseSharePopup(){
        streamPage.clickCloseSharePopup();
    }

    @And("User write a youtube video link on write here textbox")
    public void userInputLinkYouTube(){
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        linkYoutube = "https://www.youtube.com/watch?v=LLsa6iNztN0  (Youtube Tips #" + randomNum +")";
        streamPage.inputLinkYouTube(linkYoutube);
    }

    @And("User write message {string}")
    public void userWriteMessage(String message){
            streamPage.writeMessage(message);
        }

    @And("User click News tab")
    public void userClickNewsTab(){
        streamPage.clickNewsTab();
    }

    @And("User click Reports tab")
    public void userClickReportsTab(){
        streamPage.clickReportsTab();
    }

    @And("User click Predictions tab")
    public void userClickPredictionsTab(){
        streamPage.clickPredictionsTab();
    }

    @And("User click Chart tab")
    public void userClickChartTab(){
        streamPage.clickChartsTab();
    }

    @And("User click icon three dots on top right screen")
    public void userClickThreeDotsProfile(){
        profilePage.userClickThreeDotsProfile();
    }

    @And("User click blok")
    public void userClickBlok(){
        profilePage.clickBlock();
    }

    @And("User click block on pop up screen")
    public void clickBlockOnPopup(){
        profilePage.clickBlockOnPopup();
    }

    @Then("User click block on pop up screen and get notification")
    public void userClicksBlockAndGetNotif() {
        profilePage.clickBlockOnPopup();
//        Assert.assertEquals(notif,profilePage.getNotifBlock());
    }


    @And("No result of username")
    public void economicTableIsDisplayed() {
        Assert.assertTrue(searchPage.isNoResultDisplayed());
    }

    @Then("User get screen notification {string}")
    public void getNotification(String notif){
        Assert.assertEquals(notif, searchPage.getNotification());
    }

    @Then("User can't see yongkinew comment on kimhyuna post")
    public void isNoComment(){
        streamPage.isNoComment();
    }

    @And("User close comment popup")
    public void userCloseComment(){
        streamPage.clickCloseCommentPopup();
    }

    @Then("User get copy link notification {string}")
    public void userGetNotifCopyLink(String notif){
        Assert.assertEquals(notif, streamPage.getNotifLinkCopied());
    }
}
