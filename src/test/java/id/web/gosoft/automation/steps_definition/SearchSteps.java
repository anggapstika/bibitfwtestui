package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.SearchPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class SearchSteps {
    SearchPage searchPage = new SearchPage();

    @And("User search for {string}")
    public void userSearchFor(String query) {
        searchPage.inputSearchQuery(query);
    }

    @And("User search for {string} and enter")
    public void userSearchProfileName(String query) {
        searchPage.inputSearchProfile(query);
    }

    @And("User click first search result")
    public void userClickFirstSearchResult() {
        searchPage.clickFirstSearchResult();
    }

    @Then("Company {string} is displayed in the search result")
    public void companyIsDisplayedInTheSearchResult(String company) {
        Assert.assertEquals(company, searchPage.getCompanySearchResult());
    }

    @And("User is at {string} company page")
    public void userIsAtCompanyPage(String company) {
        Assert.assertEquals(company, searchPage.getCompanyName());
    }

    @Then("User {string} is displayed in the search result")
    public void userIsDisplayedInTheSearchResult(String name) {
        Assert.assertEquals(name, searchPage.getUserSearchResult());
    }

    @And("User is at {string} profile page")
    public void userIsAtProfilePage(String name) {
        Assert.assertEquals(name, searchPage.getProfileName());
    }

    @Then("Catalog {string} is displayed in the search result")
    public void catalogIsDisplayedInTheSearchResult(String catalog) {
        Assert.assertEquals(catalog, searchPage.getCatalogSearchResult());
    }

    @Then("User is at {string} catalog page")
    public void userIsAtCatalogPage(String catalog) {
        Assert.assertEquals(catalog, searchPage.getCatalogName());
    }

    @Then("Insider {string} is displayed in the search result")
    public void insiderIsDisplayedInTheSearchResult(String insider) {
        Assert.assertEquals(insider, searchPage.getInsiderSearchResult());
    }

    @When("User click first insider result")
    public void userClickFirstInsiderResult() {
        searchPage.clickFirstInsiderResult();
    }

    @And("User is at {string} insider page")
    public void userIsAtInsiderPage(String insider) {
        Assert.assertEquals(insider, searchPage.getInsiderName());
    }

    @And("User can see verified badge user profile on search")
    public void verifiedBadgeUserIsDisplayedInSearch() {
        Assert.assertTrue(searchPage.isVerifiedBadgeDisplayedOnSearch());
    }

}
