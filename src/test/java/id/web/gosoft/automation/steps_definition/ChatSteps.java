package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.ChatPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

public class ChatSteps {
    MainPage mainPage = new MainPage();
    ChatPage chatPage = new ChatPage();
    String currentMessage;

    @And("User click chat menu")
    public void userClickChatMenu() {
        mainPage.clickChatMenu();
    }

    @And("User start a new chat with {string}")
    public void userStartANewChatWith(String username) {
        chatPage.clickNewChatMenu();
        chatPage.inputRecipient(username);

        chatPage.clickStartChat();
    }

    @And("User input random message")
    public void userInputRandomMessage() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        currentMessage = "Hello bro, saya kemarin profit IDR " + randomNum;
        chatPage.inputMessage(currentMessage);
    }

    @And("User sends chat")
    public void userSendsChat() {
        chatPage.clickSendChat();
    }

    @Then("Correct message is sent to recipient")
    public void correctMessageIsSentToRecipient() {
        Assert.assertEquals(currentMessage, chatPage.getMyRecentMessage());
    }

    @And("User choose emoji {string}")
    public void userChooseEmoji(String emoji) {
        chatPage.clickChooseEmoji();
        chatPage.searchEmoji(emoji);
    }

    @Then("Correct {string} emoji message is sent to recipient")
    public void correctEmojiMessageIsSentToRecipient(String emoji) {
        Assert.assertTrue(chatPage.isEmojiExistsInMyNewestMessage(emoji));
    }

    @And("User upload an image")
    public void userUploadAnImage() {
        chatPage.clickAttachFile();
        chatPage.selectImage();
    }

    @And("Image is attached to the chat")
    public void imageIsAttachedToTheChat() {
        Assert.assertTrue(chatPage.isImageExistsInMyNewestMessage());
    }


    @And("User choose GIF {string}")
    public void userChooseGIF(String gif) {
        chatPage.clickChooseGif();
        chatPage.searchGif(gif);
    }

    @And("Gif is attached to the chat")
    public void gifIsAttachedToTheChat() {
        Assert.assertTrue(chatPage.isGifExistsInMyNewestMessage());
    }

    @And("User clears chat")
    public void userClearsChat() {
        chatPage.clickChatMore();
        chatPage.clickClearChat();
        chatPage.clickConfirmClearOrDelete();
    }

    @Then("Chat is empty")
    public void chatIsEmpty() {
        Assert.assertTrue(chatPage.isChatEmpty());
    }

    @And("User deletes chat")
    public void userDeletesChat() {
        chatPage.clickChatMore();
        chatPage.clickDeleteChat();
        chatPage.clickConfirmClearOrDelete();
    }

    @Then("{string} is not present recent chats")
    public void isNotPresentRecentChats(String username) {
        Assert.assertFalse(chatPage.isUserInRecentChats(username));
    }
}
