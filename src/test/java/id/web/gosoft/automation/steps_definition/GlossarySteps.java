package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.GlossaryPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.List;

public class GlossarySteps {

    MainPage mainPage = new MainPage();
    GlossaryPage glossaryPage = new GlossaryPage();

    @And("User click glossary menu")
    public void userClickGlossaryMenu() {
        mainPage.clickGlossaryMenu();
    }

    @And("User search glossary {string}")
    public void userSearchGlossary(String glossary) {
        glossaryPage.inputGlossarySearch(glossary);
        glossaryPage.clickFirstSearchResult();
    }

    @Then("Glossary index {string} is highlighted")
    public void glossaryIndexIsHighlighted(String index) {
        Assert.assertEquals(index, glossaryPage.getActiveGlossaryIndex());
    }

    @And("Glossary row {string} is active")
    public void glossaryRowIsActive(String row) {
        Assert.assertEquals(row, glossaryPage.getActiveGlossaryRow());
    }

    @And("User is shown {string} glossary page")
    public void userIsShownGlossaryPage(String title) {
        Assert.assertEquals(title, glossaryPage.getSearchGlossary());
    }

    @And("User click glossary at index {string}")
    public void userClickGlossaryAtIndex(String index) {
        glossaryPage.clickGlossaryIndex(index);
    }

    @Then("Every glossary row starts with the letter {string}")
    public void everyGlossaryRowStartsWithTheLetter(String index) {
        List<String> list = glossaryPage.getListGlossaryRowFirstLetter();

        for (String firstLetter : list) {
            Assert.assertEquals(index, firstLetter);
        }
    }

}
