package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.EarningsPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class EarningsSteps {
    MainPage mainPage = new MainPage();
    EarningsPage earningsPage = new EarningsPage();

    @And("User click earnings menu")
    public void userClickEarningsMenu() {
        mainPage.clickEarningsMenu();
    }

    @And("User select sector")
    public void userSelectSector() {
        earningsPage.selectSector();
    }

    @And("User input symbol on Earning Feature as {string}")
    public void userInputSymbolOnEarning(String symbol) {
        earningsPage.inputSymbol(symbol);
    }

    @And("User click search")
    public void userClickSearch() {
        earningsPage.clickSearch();
    }

    @Then("User get symbol {string}")
    public void userGetSymbol(String seearchresult) {
        Assert.assertEquals(seearchresult, earningsPage.getSearch());
    }
}
