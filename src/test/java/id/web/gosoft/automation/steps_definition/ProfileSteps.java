package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.ProfilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class ProfileSteps {
    ProfilePage profilePage = new ProfilePage();
    String name;
    String email;
    String biography;

    String location;
    String gender;

    String day;
    String month;
    String year;

    String website;
    String address;

    int randomNum;

    @And("User click edit profile")
    public void userClickEditProfile() {
        profilePage.clickEditProfile();
    }

    @And("User click edit profile tab")
    public void userClickEditProfileTab() {
        profilePage.clickEditProfileTab();
    }

    @And("User input unique name")
    public void userInputUniqueName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, 999);
        name = "angga" + randomNum;
        profilePage.inputName(name);
    }

    @And("User input unique email")
    public void userInputUniqueEmail() {
        email = "angga" + randomNum + "@outlook.com";
        profilePage.inputEmail(email);
    }

    @And("User click hide email checkbox")
    public void userClickHideEmailCheckbox() {
        profilePage.clickCheckboxHideEmail();
    }

    @And("User input unique biography")
    public void userInputUniqueBiography() {
        biography = "Pemain saham #" + randomNum;
        profilePage.inputBiography(biography);
    }

    @And("User set location to {string}")
    public void userSetLocationTo(String location) {
        this.location = location;
        profilePage.selectLocation(location);
    }

    @And("User set gender to {string}")
    public void userSetGenderTo(String gender) {
        this.gender = gender;
        profilePage.selectGender(gender);
    }

    @And("User set birthday to {string} {string} {string}")
    public void userSetBirthdayTo(String day, String month, String year) {
        this.day = day;
        this.month = month;
        this.year = year;

        profilePage.selectDay(day);
        profilePage.selectMonth(month);
        profilePage.selectYear(year);
    }

    @And("User set unique website")
    public void userSetUniqueWebsite() {
        website = "stockbit" + randomNum + ".com";
        profilePage.inputWebsite(website);
    }

    @And("User set unique address")
    public void userSetUniqueAddress() {
        address = "Jalan Sukarela, Cijantung No. " + randomNum;
        profilePage.inputAddress(address);
    }

    @And("User click save profile")
    public void userClickSaveProfile() {
        profilePage.clickSaveSetting();
    }

    @And("User public profile has changed")
    public void userPublicProfileHasChanged() {
        Assert.assertEquals(name, profilePage.getEditName());
//        Assert.assertEquals(email, profilePage.getEditEmail());
        Assert.assertTrue(profilePage.isHideEmailChecked());
        Assert.assertEquals(biography, profilePage.getEditBiography());

        Assert.assertEquals(location, profilePage.getEditLocation());
        Assert.assertEquals(gender, profilePage.getEditGender());

        Assert.assertEquals(day, profilePage.getEditDay());
        Assert.assertEquals(month, profilePage.getEditMonth());
        Assert.assertEquals(year, profilePage.getEditYear());

        Assert.assertEquals(website, profilePage.getEditWebsite());
        Assert.assertEquals(address, profilePage.getEditAddress());

    }

    @And("User click edit preferences tab")
    public void userClickEditPreferencesTab() {
        profilePage.clickEditPreferencesTab();
    }

    @And("User choose {string} region")
    public void userChooseRegion(String region) {
        profilePage.pickRegion(region);
    }

    @Then("Region is set as {string}")
    public void regionIsSetAs(String region) {
        Assert.assertTrue(profilePage.isRegionSelected(region));
    }

    @And("User click edit password tab")
    public void userClickEditPasswordTab() {
        profilePage.clickEditPasswordTab();
    }


    @And("User input current password {string}")
    public void userInputCurrentPassword(String password) {
        profilePage.inputOldPassword(password);
    }

    @And("User input new password {string}")
    public void userInputNewPassword(String password) {
        profilePage.inputNewPassword(password);
    }

    @And("User input verify password {string}")
    public void userInputVerifyPassword(String password) {
        profilePage.inputVerifyPassword(password);
    }

    @Then("Password is updated")
    public void passwordIsUpdated() {
        Assert.assertTrue(profilePage.isPasswordUpdatedAlertShown());
    }

    @Then("Incorrect current password alert is shown")
    public void incorrectCurrentPasswordAlertIsShown() {
        Assert.assertTrue(profilePage.isIncorrectCurrentPasswordAlertShown());
    }

    @Then("New password too short alert is shown")
    public void newPasswordTooShortAlertIsShown() {
        Assert.assertTrue(profilePage.isNewPasswordTooShortShown());
    }

    @Then("Verify password too short alert is shown")
    public void verifyPasswordTooShortAlertIsShown() {
        Assert.assertTrue(profilePage.isVerifyPasswordTooShortShown());
    }

    @Then("Password not match alert is shown")
    public void passwordNotMatchAlertIsShown() {
        Assert.assertTrue(profilePage.isPasswordNotMatchShown());
    }

    @And("User click edit notifications tab")
    public void userClickEditNotificationsTab() {
        profilePage.clickEditNotificationsTab();
    }

    @And("User click email accordion")
    public void userClickEmailAccordion() {
//        profilePage.clickNotifAccordion(NOTIF_ACCORDION_EMAIL);
        profilePage.clickEmailNotification();
    }

    @And("User set email {string} checkbox to {string}")
    public void userSetEmailCheckboxTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        profilePage.clickNotifCheckbox(NOTIF_ACCORDION_EMAIL, checkbox, boolStatus);
    }

    @Then("Email {string} checkbox is set to {string}")
    public void emailCheckboxIsSetTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        Assert.assertEquals(boolStatus, profilePage.isNotifChecked(NOTIF_ACCORDION_EMAIL, checkbox));
    }

    @And("User click mobile accordion")
    public void userClickMobileAccordion() {
//        profilePage.clickNotifAccordion(NOTIF_ACCORDION_MOBILE);
        profilePage.clickMobileNotification();
    }

    @And("User set mobile {string} checkbox to {string}")
    public void userSetMobileCheckboxTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        profilePage.clickNotifCheckbox(NOTIF_ACCORDION_MOBILE, checkbox, boolStatus);
    }

    @Then("Mobile {string} checkbox is set to {string}")
    public void mobileCheckboxIsSetTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        Assert.assertEquals(boolStatus, profilePage.isNotifChecked(NOTIF_ACCORDION_MOBILE, checkbox));
    }

    @And("User click desktop accordion")
    public void userClickDesktopAccordion() {
//        profilePage.clickNotifAccordion(NOTIF_ACCORDION_DESKTOP);
        profilePage.clickDekstopNotification();
    }

    @And("User set desktop {string} checkbox to {string}")
    public void userSetDesktopCheckboxTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        profilePage.clickNotifCheckbox(NOTIF_ACCORDION_DESKTOP, checkbox, boolStatus);
    }

    @And("Desktop {string} checkbox is set to {string}")
    public void desktopCheckboxIsSetTo(String checkbox, String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        Assert.assertEquals(boolStatus, profilePage.isNotifChecked(NOTIF_ACCORDION_DESKTOP, checkbox));
    }

    @And("User click edit privacy tab")
    public void userClickEditPrivacyTab() {
        profilePage.clickEditPrivacyTab();
    }

    @And("User set switch discoverable by contacts to {string}")
    public void userSetSwitchDiscoverableByContactsTo(String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        profilePage.clickSwitchDiscoverableByContacts(boolStatus);
    }

    @Then("User discoverable by contacts status is {string}")
    public void userDiscoverableByContactsStatusIs(String targetStatus) {
        boolean boolStatus = Boolean.parseBoolean(targetStatus);
        Assert.assertEquals(boolStatus, profilePage.isSwitchDiscoverableByContactsActive());
    }

    @And("User block the other user")
    public void userBlockTheOtherUser() {
        profilePage.blockUser();
    }

    @And("User click edit blocked list tab")
    public void userClickEditBlockedListTab() {
        profilePage.clickEditBlockedListTab();
    }

    @And("User unblock {string}")
    public void userUnblock(String user) {
        profilePage.clickUnblockUser(user);
    }

    @Then("User is unblocked")
    public void userIsUnblocked() {
        Assert.assertTrue(profilePage.isUserUnblocked());
    }

    @And("User can see verified badge in post")
    public void verifiedBadgeUserIsDisplayedInPost() {
        Assert.assertTrue(profilePage.isVerifiedBadgeDisplayedOnPost());
    }

    @And("User click show all avatars")
    public void userClickShowAllAvatars(){
        profilePage.clickShowAll();
    }

    @And("User click random avatar")
    public void userClickRandomAvatar(){
        profilePage.clickRandomAvatar();
    }

    @And("User click save")
    public void userClickSave(){
        profilePage.clickSave();
    }

    @And("User choose male section")
    public void userChooseMaleSection(){
        profilePage.chooseMale();
    }

    @And("User choose female section")
    public void userChooseFemaleSection(){
        profilePage.chooseFemale();
    }

    @And("User click remove profile picture")
    public void userClickRemoveProfile(){
        profilePage.clickRemoveProfile();
    }

    @And("User click remove Avatar")
    public void userClickRemoveAvatar(){
        profilePage.clickRemoveAvatar();
    }

    @Then("Avatar profile changed to a random 3D avatar")
    public void AvatarIsDisplayed(){
        Assert.assertTrue(profilePage.IsAvatarDisplayed());
    }

    @And("User click edit symbol")
    public void clickEditSymbol(){profilePage.clickEditSimbol();}

    @And("User click public profile")
    public void clickPublicProfile(){profilePage.clickPublicProfile();}

    @And("User click back to profile view")
    public void clickBackToProfileView(){profilePage.clickBackProfileView();}

    @And("User click submit password")
    public void clickSubmitPassword(){profilePage.clickSubmitOldPassword();}

    @And("User click profile picture icon")
    public void clickProfileIcon(){profilePage.clickProfileIcon();}

}
