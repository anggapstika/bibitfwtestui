package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.CompanyPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import id.web.gosoft.automation.pages.stockbit.OrderbookPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class OrderbookSteps {
    MainPage mainPage = new MainPage();
    OrderbookPage orderbookPage = new OrderbookPage();
    CompanyPage companyPage = new CompanyPage();

    int randomNum;
    String orderbookListName;
    int listSize;

    @And("User click orderbook menu")
    public void userClickOrderbookMenu() {
        mainPage.clickOrderbookMenu();
    }

    @And("User click create orderbook header")
    public void userClickCreateOrderbookHeader() {
        orderbookPage.clickOrderbookHeader();
    }

    @And("User click create new orderbook list")
    public void userClickCreateNewOrderbookList() {
        orderbookPage.scrollToCreateListBtn();
        orderbookPage.clickCreateListBtn();
    }

    @And("User input unique orderbook list name")
    public void userInputUniqueOrderbookListName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        orderbookListName = "Orderbook " + randomNum;
        orderbookPage.inputListName(orderbookListName);
    }

    @And("User click confirm orderbook list name")
    public void userClickConfirmOrderbookListName() {
        orderbookPage.clickConfirmCreateBtn();
    }

    @Then("Orderbook list is created")
    public void orderbookListIsCreated() {
        Assert.assertEquals(orderbookListName, orderbookPage.getOrderbookListHeader());
        orderbookPage.clickOrderbookHeader();
        orderbookPage.waitABit(3);
        orderbookPage.scrollToCreateListBtn();
        Assert.assertEquals(orderbookListName, orderbookPage.getLatestOrderbookListName());
    }

    @And("User click save orderbook list button")
    public void userClickSaveOrderbookListButton() {
        orderbookPage.clickSaveOrderbookListBtn();
    }

    @And("User input new unique orderbook list name")
    public void userInputNewUniqueOrderbookListName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        orderbookListName = "Orderbook " + randomNum;
        orderbookPage.inputNewListName(orderbookListName);
    }

    @And("User click confirm new orderbook list name")
    public void userClickConfirmNewOrderbookListName() {
        orderbookPage.clickConfirmUpdateBtn();
    }

    @When("User click edit latest orderbook list")
    public void userClickEditLatestOrderbookList() {
        orderbookPage.editLatestOrderbookList();
    }

    @Then("Orderbook list is updated")
    public void orderbookListIsUpdated() {
        String message = "Orderbook list name changed";
        orderbookPage.scrollToTop();
        Assert.assertEquals(message, orderbookPage.getSuccessAlertMessage());
//        orderbookPage.clickOrderbookHeader();
//        orderbookPage.waitABit(3);
//        orderbookPage.scrollToCreateListBtn();
//        Assert.assertEquals(orderbookListName, orderbookPage.getLatestOrderbookListName());
    }

    @And("User input same orderbook name")
    public void userInputSameOrderbookName() {
        orderbookPage.inputListName(orderbookListName);
    }

    @Then("Latest orderbook list name is correct")
    public void latestOrderbookListNameIsCorrect() {
        orderbookPage.scrollToCreateListBtn();
        Assert.assertEquals(orderbookListName, orderbookPage.getLatestOrderbookListName());
    }

    @When("User delete latest orderbook list")
    public void userDeleteLatestOrderbookList() {
        orderbookPage.scrollToCreateListBtn();
        orderbookPage.deleteLatestOrderbookList();
        orderbookPage.clickConfirmDeleteBtn();
    }

    @Then("Orderbook list is deleted")
    public void orderbookListIsDeleted() {
        String alert = "Remove successful";
        Assert.assertEquals(alert, orderbookPage.getSuccessAlertMessage());
    }

    @And("User click add orderbook button")
    public void userClickAddOrderbookButton() {
        orderbookPage.clickAddOrderbookBtn();
        orderbookPage.clickConfirmAddOrderbookBtn();
    }

    @And("User input orderbook symbol {string}")
    public void userInputOrderbookSymbol(String symbol) {
        orderbookPage.scrollToBottom();
        orderbookPage.inputOrderbookSymbol(symbol);
    }

    @And("User click first suggestion")
    public void userClickFirstSuggestion() {
        orderbookPage.selectFirstSymbolSuggestion();
    }

    @Then("Latest orderbook is created with name {string}")
    public void latestOrderbookIsCreatedWithName(String symbol) {
        orderbookPage.scrollToBottom();
        Assert.assertEquals(symbol, orderbookPage.getLatestOrderbookName());
    }

    @And("Latest orderbook realtime table is available")
    public void latestOrderbookRealtimeTableIsAvailable() {
        Assert.assertTrue(orderbookPage.isLatestTablePresent());
    }

    @And("Latest orderbook realtime table is not available")
    public void latestOrderbookRealtimeTableIsNotAvailable() {
        Assert.assertFalse(orderbookPage.isLatestTablePresent());
    }

    @And("Orderbook suggestion doesn't appear")
    public void orderbookSuggestionDoesnTAppear() {
        Assert.assertFalse(orderbookPage.isSymbolSuggestionPresent());
    }

    @And("User get current orderbook list size")
    public void userGetCurrentOrderbookListSize() {
        listSize = orderbookPage.getOrderbookListSize();
    }

    @And("User open orderbook dropdown")
    public void userOpenOrderbookDropdown() {
        orderbookPage.clickLatestOrderbookDropmenu();
    }

    @And("User delete latest orderbook")
    public void userDeleteLatestOrderbook() {
        orderbookPage.clickRemoveOrderbook();
    }

    @And("Orderbook list size is added {int}")
    public void orderbookListSizeIsAdded(int added) {
        int newSize = listSize + added;
        Assert.assertEquals(newSize, orderbookPage.getOrderbookListSize());
    }

    @Then("Orderbook list size is the same as before")
    public void orderbookListSizeIsTheSameAsBefore() {
        Assert.assertEquals(listSize, orderbookPage.getOrderbookListSize());
    }

    @And("User click orderbook Company Stream dropdown menu")
    public void userClickOrderbookCompanyStreamDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_STREAM);
    }

    @Then("User is redirected to orderbook {string} Company Stream")
    public void userIsRedirectedToOrderbookCompanyStream(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_STREAM));
    }

    @And("User click orderbook Key Stats dropdown menu")
    public void userClickOrderbookKeyStatsDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_KEY_STATS);
    }

    @Then("User is redirected to orderbook {string} Key Stats")
    public void userIsRedirectedToOrderbookKeyStats(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_KEY_STATS));
    }

    @And("User click orderbook Financials dropdown menu")
    public void userClickOrderbookFinancialsDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_FINANCIALS);
    }

    @Then("User is redirected to orderbook {string} Financials")
    public void userIsRedirectedToOrderbookFinancials(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_FINANCIALS));
    }

    @And("User click orderbook Fundachart dropdown menu")
    public void userClickOrderbookFundachartDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_FUNDACHART);
    }

    @Then("User is redirected to orderbook {string} Fundachart")
    public void userIsRedirectedToOrderbookFundachart(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_FUNDACHART));
    }

    @And("User click orderbook Seasonality dropdown menu")
    public void userClickOrderbookSeasonalityDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_SEASONALITY);
    }

    @Then("User is redirected to orderbook {string} Seasonality")
    public void userIsRedirectedToOrderbookSeasonality(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_SEASONALITY));
    }

    @And("User click orderbook Target Price dropdown menu")
    public void userClickOrderbookTargetPriceDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_TARGET_PRICE);
    }

    @Then("User is redirected to orderbook {string} Target Price")
    public void userIsRedirectedToOrderbookTargetPrice(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        companyPage.clickTabMore();
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_CONSENSUS));
    }

    @And("User click orderbook Company Profile dropdown menu")
    public void userClickOrderbookCompanyProfileDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_PROFILE);
    }

    @Then("User is redirected to orderbook {string} Company Profile")
    public void userIsRedirectedToOrderbookCompanyProfile(String name) {
        mainPage.toLatestTab();
        Assert.assertEquals(name, companyPage.getCompanyName());
        companyPage.clickTabMore();
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_PROFILE));
    }

    @And("User click orderbook Chartbit dropdown menu")
    public void userClickOrderbookChartbitDropdownMenu() {
        orderbookPage.clickMenu(ORDERBOOK_DROPDOWN_CHARTBIT);
    }

    @Then("User is redirected to orderbook {string} Chartbit")
    public void userIsRedirectedToOrderbookChartbit(String name) {
        mainPage.toLatestTab();
        mainPage.waitABit(5);
        Assert.assertEquals(name, orderbookPage.getChartbitCompanyName());
    }
}
