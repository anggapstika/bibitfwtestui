package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.AcademyPage;
import io.cucumber.java.en.And;
import org.junit.Assert;

public class AcademySteps {
    AcademyPage academyPage = new AcademyPage();

    @And("User click menu academy")
    public void userClickAcademy(){
        academyPage.clickAcademy();
    }

    @And("User click module 1")
    public void userClickModule1() {
        academyPage.clickModule1();
    }

    @And("User click chapter 1")
    public void userClickChapter1(){
        academyPage.clickChapter1();
    }

    @And("User click lesson 1")
    public void userClickLesson1(){
        academyPage.clicklesson1();
    }

    @And("User click video play button")
    public void userClickPlayButton(){
        academyPage.userClickPlayVideo();
    }

    @And("User can watch the video while read the lesson")
    public void userCanReadTheLesson(){
        Assert.assertTrue(academyPage.lessonIsDisplayed());
    }



}
