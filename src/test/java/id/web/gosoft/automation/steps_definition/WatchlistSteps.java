package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.MainPage;
import id.web.gosoft.automation.pages.stockbit.WatchlistPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

public class WatchlistSteps {
    MainPage mainPage = new MainPage();
    WatchlistPage watchlistPage = new WatchlistPage();

    int randomNum;
    String newWatchlistName = "";
    String oldWatchlistName = "";


    @And("User click watchlist menu")
    public void userClickWatchlistMenu() {
        mainPage.clickWatchlistMenu();
    }

    @And("User remove {string} from watchlist if exists")
    public void userRemoveFromWatchlistIfExists(String symbol) {
        watchlistPage.removeSymbolFromWatchlist(symbol);
    }

    @And("User input {string} into search symbol")
    public void userInputIntoSearchSymbol(String symbol) {
        watchlistPage.searchSymbolWatchlist(symbol);
    }

    @And("User click first symbol")
    public void userClickFirstSymbol() {
        watchlistPage.clickFirstSymbolResult();
    }

    @Then("Symbol {string} is added to watchlist")
    public void symbolIsAddedToWatchlist(String symbol) {
        Assert.assertTrue(watchlistPage.isSymbolPresentOnWatchlist(symbol));
    }

    @And("User add column {string} to watchlist")
    public void userAddColumnToWatchlist(String column) {
        watchlistPage.clickAddFinancialColumn();
        watchlistPage.searchColumn(column);
    }

    @Then("Column {string} is added to watchlist")
    public void columnIsAddedToWatchlist(String column) {
        Assert.assertEquals(column, watchlistPage.getFirstNewColumn());
    }

    @Then("User gets error {string} already exists")
    public void userGetsErrorAlreadyExists(String column) {
//        String error = "Column '" + column + "' already exists";
        String error = "Column '" + column + "' has already been added";

        Assert.assertEquals(error, mainPage.getAlertMessage());
    }

    @And("User remove column from watchlist if exists")
    public void userRemoveColumnFromWatchlistIfExists() {
        watchlistPage.removeColumnIfExists();
    }

    @And("User remove column from watchlist")
    public void userRemoveColumnFromWatchlist() {
        watchlistPage.removeColumn();
    }

    @Then("Financial column is deleted")
    public void financialColumnIsDeleted() {
        Assert.assertFalse(watchlistPage.isFinancialColumnIsDeleted());
    }

    @And("User open watchlist dropdown")
    public void userOpenWatchlistDropdown() {
        watchlistPage.openWatchlistDropdown();
    }

    @And("User create new watchlist")
    public void userCreateNewWatchlist() {
        oldWatchlistName = newWatchlistName;

        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        newWatchlistName = "Watchlist " + randomNum;

        watchlistPage.clickCreateWatchlistMenu();
        watchlistPage.inputWatchlistName(newWatchlistName);
        watchlistPage.clickCreateWatchlist();
    }

    @Then("Watchlist is created")
    public void watchlistIsCreated() {
        Assert.assertEquals(newWatchlistName, watchlistPage.getCurrentWatchlistName());
    }

    @And("User rename watchlist")
    public void userRenameWatchlist() {
        watchlistPage.scrollToBottomOfWatchlist();
        watchlistPage.clickEditWatchList(newWatchlistName);

        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        newWatchlistName = "Watchlist " + randomNum;

        watchlistPage.inputEditWatchlist(newWatchlistName);
        watchlistPage.clickConfirmEdit();
    }

    @Then("Watchlist is renamed")
    public void watchlistIsRenamed() {
        Assert.assertEquals(newWatchlistName, watchlistPage.getCurrentWatchlistName());
    }

    @And("User rename new watchlist with old name")
    public void userRenameNewWatchlistWithOldName() {
        watchlistPage.scrollToBottomOfWatchlist();
        watchlistPage.clickEditWatchList(newWatchlistName);
        watchlistPage.inputEditWatchlist(oldWatchlistName);
        watchlistPage.clickConfirmEdit();
    }

    @Then("Watchlist is not renamed")
    public void watchlistIsNotRenamed() {
        watchlistPage.scrollToBottomOfWatchlist();
        Assert.assertTrue(watchlistPage.isWatchlistPresent(newWatchlistName));
    }

    @And("User remove created watchlist")
    public void userRemoveCreatedWatchlist() {
        watchlistPage.scrollToBottomOfWatchlist();
        watchlistPage.clickDeleteWatchlist(newWatchlistName);
    }

    @Then("Watchlist is deleted")
    public void watchlistIsDeleted() {
        watchlistPage.scrollToBottomOfWatchlist();
        Assert.assertFalse(watchlistPage.isWatchlistPresent(newWatchlistName));
    }
}
