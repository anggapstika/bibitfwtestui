package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.OrderPage;
import id.web.gosoft.automation.pages.stockbit.ReferralPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class ReferralSteps {
    ReferralPage referralPage = new ReferralPage();
    OrderPage orderPage = new OrderPage();

    @And("User click menu kode referral")
    public void userClickMenuKodeReferral() {
        referralPage.clickMenuReferral();
    }

    @And("User click edit kode referral")
    public void userClickEditKodeReferral() {
        referralPage.clickEditReferral();
    }

    @And("User input kode referral {string}")
    public void userInputKodeReferral(String code) {
        referralPage.inputReferral(code);
    }

    @And("User click simpan kode referral")
    public void userClickSimpanKodeReferral() {
        referralPage.clickSaveReferral();
    }

    @Then("User get alert {string}")
    public void userGetAlert(String alert) {
        Assert.assertEquals(alert, referralPage.getSuccessAlertMessage());
    }

    @And("User click share kode referral")
    public void userClickShareKodeReferral() {
        referralPage.clickShareReferral();
    }

    @Then("Share kode referral modal is shown")
    public void shareKodeReferralModalIsShown() {
        Assert.assertTrue(referralPage.isModalShareReferralPresent());
    }

    @And("User click copy kode referral")
    public void userClickCopyKodeReferral() {
        referralPage.clickCopyReferral();
    }

    @Then("Kode referral is copied to clipboard")
    public void kodeReferralIsCopiedToClipboard() {
        Assert.assertEquals(referralPage.getCurrentReferralCode(), referralPage.getCopiedReferralCode());
    }

    @And("User click view redeem list")
    public void userClickViewRedeemList() {
        referralPage.clickViewRedeemList();
    }

    @And("User click tab {string}")
    public void userClickTab(String tab) {
        referralPage.clickRedeemTab(tab);
    }

    @And("User redeem first coupon")
    public void userRedeemFirstCoupon() {
        referralPage.clickRedeemFirstCoupon();
    }

    @Then("User is redirected to stockbit securitas challenge pin")
    public void userIsRedirectedToStockbitSecuritasChallengePin() {
        Assert.assertTrue(referralPage.isAtChallengePinPage());
    }

//    @And("User input stockbit sekuritas pin {string}")
//    public void userInputStockbitSekuritasPin(String pin) {
//        orderPage.inputStockbitSekuritasPin(pin);
//    }
//
//    @And("User click submit stockbit sekuritas pin")
//    public void userClickSubmitStockbitSekuritasPin() {
//        orderPage.clickConfirmStockbitSekuritasPin();
//    }

    @Then("User is shown modal Tunggu Dulu")
    public void userIsShownModalTungguDulu() {
        Assert.assertTrue(referralPage.isShownModalTungguDulu());
    }

    @Then("User is on referral history page")
    public void userIsOnReferralHistoryPage() {
        Assert.assertTrue(referralPage.isOnReferralHistoryPage());
    }
}
