package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class DiscoverPeopleSteps {

    DiscoverPeoplePage discoverPeoplePage = new DiscoverPeoplePage();

    @And("User scroll to trending section")
    public void userScrollToTrendingSection(){
        discoverPeoplePage.scrollToTrendingSection();
    }

    @And("User click +Discover more investors")
    public void userClickAddDiscover(){
        discoverPeoplePage.clickAddDiscover();
    }

    @And("User click button {string} to one of contact")
    public void userClickFollow(String Follow){
        Assert.assertEquals(Follow, discoverPeoplePage.getFollowText());
        discoverPeoplePage.clickFollow();
    }

    @And("User click button {string} to one of contact followers")
    public void userClickFollowers(String Followers){
        Assert.assertEquals(Followers, discoverPeoplePage.getFollowersText());
        discoverPeoplePage.clickOneOfFollowers();
    }

    @Then("Button follow is changes to {string}")
    public void isChangesToFollowing(String Following){
        Assert.assertEquals(Following, discoverPeoplePage.getFollowingText());
    }

    @And("User click button {string} in previous follow")
    public void userClickFollowingInPreviousFollow(String Following){
        Assert.assertEquals(Following, discoverPeoplePage.getFollowersFollowingText());
        discoverPeoplePage.clickOneOfFollowers();
    }

    @Then("Button follow of followers is changes to {string}")
    public void isFollowersChangesToFollowing(String Following){
        Assert.assertEquals(Following, discoverPeoplePage.getFollowersFollowText());}

    @And("User click button {string}")
    public void userClickFollowing(String Following){
        Assert.assertEquals(Following, discoverPeoplePage.getFollowingText());
        discoverPeoplePage.clickFollowing();
    }

    @Then("Followed button changed as {string}")
    public void isChangesToFollow(String Follow){
        Assert.assertEquals(Follow, discoverPeoplePage.getFollowText());
    }

    @Then("Followed button of followers changed as {string}")
    public void isChangesfollowersToFollow(String Follow){
        Assert.assertEquals(Follow, discoverPeoplePage.getFollowersFollowText());
    }

    @Then("Followed button in followers changed as {string}")
    public void isFollowersChangesToFollow(String Follow){
        Assert.assertEquals(Follow, discoverPeoplePage.getFollowersFollowText());
    }

    @And("User search for {string} text box in discover and enter")
    public void userSearchDiscover(String discover){
        discoverPeoplePage.inputSearchDiscover(discover);
    }

    @And("{string} is displayed in the search result")
    public void searchResultIsDisplayed(String result){
        Assert.assertEquals(result, discoverPeoplePage.getSearchResult());
    }

    @And("User scroll until section suggestion for you and see all")
    public void userScrollUntilSuggestion(){
        discoverPeoplePage.scrollUntilSuggestion();
    }

    @And("User redirect to suggestion list in discover people menu")
    public void userRedirectToSuggestionDiscoverPeople(){
        Assert.assertTrue(discoverPeoplePage.isDiscoverPeopleMenu());
    }

    @And("User redirect to suggestion to follower List")
    public void userRedirectToFollowerList(){
       Assert.assertTrue(discoverPeoplePage.isFollowerListPage());
    }

    @And("User click followers")
    public void userClickFollowers(){
        discoverPeoplePage.clickFollowers();
    }

    @And("User search for {string} in follower page")
    public void userSearchFollower(String followers){
        discoverPeoplePage.inputSearchFollowers (followers);
    }

    @And("User get notification {string} Because name is wrong user")
    public void userGetNotificationSearch(String wrong){
        Assert.assertEquals(wrong, discoverPeoplePage.getNotificationSearch());
    }

    @And("user click close icon on text box")
    public void userClose(){
        discoverPeoplePage.userClickCloseSearch();
    }

    @And("User click first result follow")
    public void userClickFirstResultFollow(){
        discoverPeoplePage.clickFirstResultFollow();
    }

    @And("User {string} is displayed in the search follower result")
    public void userIsDisplayedInTheSearchFollowerResult(String name) {
        Assert.assertEquals(name, discoverPeoplePage.getUserSearchFollowerResult());
    }

}
