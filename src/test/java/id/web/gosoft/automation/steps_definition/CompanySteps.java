package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.CompanyPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class CompanySteps {
    CompanyPage companyPage = new CompanyPage();

    int randomNum;
    String chartName = "";
    String comparisonName = "";

    @And("User go to {string} company page")
    public void userGoToCompanyPage(String symbol) {
        companyPage.goToCompanyPage(symbol);
    }

    @And("User scroll to catalog section")
    public void userScrollToCatalogSection() {
        companyPage.scrollToCatalogSection();
    }

    @And("User select {string} catalog")
    public void userSelectCatalog(String catalog) {
        companyPage.clickCatalog(catalog);
    }

    @And("User scroll to company navigation")
    public void userScrollToCompanyNavigation() {
        companyPage.scrollToNavigation();
    }

    @And("User navigate to company Key Stats section")
    public void userNavigateToCompanyKeyStatsSection() {
        companyPage.clickNavTab(COMPANY_TAB_KEY_STATS);
    }

    @And("User clicks a graph icon")
    public void userClicksAGraphIcon() {
        companyPage.clickChartIcon();
    }

    @Then("User is at company Fundachart section")
    public void userIsAtCompanyFundachartSection() {
        Assert.assertTrue(companyPage.isTabActive(COMPANY_TAB_FUNDACHART));
        Assert.assertTrue(companyPage.isFundachartPresent());
    }

    @And("User navigate to company Seasonality section")
    public void userNavigateToCompanySeasonalitySection() {
        companyPage.clickNavTab(COMPANY_TAB_SEASONALITY);
    }

    @And("User set seasonality range to {string}")
    public void userSetSeasonalityRangeTo(String year) {
        companyPage.setSeasonalityRange(year);
    }

    @And("User set seasonality end year to {string}")
    public void userSetSeasonalityEndYearTo(String year) {
        companyPage.setSeasonalityEnd(year);
    }

    @And("User click show seasonality")
    public void userClickShowSeasonality() {
        companyPage.clickShowSeasonality();
    }

    @Then("Seasonality is shown from {string} with {int} year range")
    public void seasonalityIsShownFromEndWithYearYearRange(String end, int range) {
        List<String> listColYear = companyPage.getListYear();
        Assert.assertEquals(range, listColYear.size() - 1);

        String latestYear = listColYear.get(0);
        Assert.assertEquals(end, latestYear);
    }

    @And("User navigate to company Fundachart section")
    public void userNavigateToCompanyFundachartSection() {
        companyPage.clickNavTab(COMPANY_TAB_FUNDACHART);
    }

    @And("User add stock fundachart graph {string}")
    public void userAddStockFundachartGraph(String stock) {
        companyPage.chooseFundachartStock(stock);
    }

    @And("User add metric fundachart graph {string}")
    public void userAddMetricFundachartGraph(String metric) {
        companyPage.chooseFundachartMetric(metric);
    }

    @Then("Graph is displayed stock {string} and {string} with {string}")
    public void graphIsDisplayedStockAndWith(String symbol, String stock, String metric) {
        String text1 = symbol + " Price";
        String text2 = stock + " Price";
        String text3 = symbol + " " + metric;
        String text4 = stock + " " + metric;

        Assert.assertTrue(companyPage.isGraphDisplayed(text1));
        Assert.assertTrue(companyPage.isGraphDisplayed(text2));
        Assert.assertTrue(companyPage.isGraphDisplayed(text3));
        Assert.assertTrue(companyPage.isGraphDisplayed(text4));
    }

    @And("User click save fundachart")
    public void userClickSaveFundachart() {
        companyPage.clickChartSave();
    }

    @And("User click fundachart save as")
    public void userClickFundachartSaveAs() {
        companyPage.clickChartSaveAs();
    }

    @And("User input unique fundachart name")
    public void userInputUniqueFundachartName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        chartName = "Chart " + randomNum;
        companyPage.inputNewChartName(chartName);
    }

    @And("User confirm save fundachart")
    public void userConfirmSaveFundachart() {
        companyPage.clickConfirmSave();
    }

    @Then("New fundachart exists")
    public void newFundachartExists() {
        Assert.assertTrue(companyPage.isNewChartExists(chartName));
    }

    @And("User navigate to company Stream section")
    public void userNavigateToCompanyStreamSection() {
        companyPage.clickNavTab(COMPANY_TAB_STREAM);
    }

    @Then("User is on stream section")
    public void userIsOnStreamSection() {
        Assert.assertTrue(companyPage.isOnStreamSection());
    }

    @And("All stream contains symbol {string}")
    public void allStreamContainsSymbol(String symbol) {
        Assert.assertTrue(companyPage.isSymbolPresentInAll(symbol));
    }

    @And("User navigate to company Comparison section")
    public void userNavigateToCompanyComparisonSection() {
        companyPage.clickNavTab(COMPANY_TAB_COMPARISON);
    }

    @And("User search for comparison stock {string}")
    public void userSearchForComparisonStock(String stock) {
        companyPage.inputComparisonStock(stock);
    }

    @And("User click first comparison stock result")
    public void userClickFirstComparisonStockResult() {
        companyPage.clickFirstComparisonResult();
    }

    @Then("Stock {string} is shown on comparison table")
    public void stockIsShownOnComparisonTable(String stock) {
        Assert.assertEquals(stock, companyPage.getLatestComparisonStock());
    }

    @When("User delete latest comparison stock")
    public void userDeleteLatestComparisonStock() {
        companyPage.clickDeleteLatestComparisonStock();
    }

    @Then("Stock {string} is not shown on comparison table")
    public void stockIsNotShownOnComparisonTable(String stock) {
        Assert.assertNotEquals(stock, companyPage.getLatestComparisonStock());
    }

    @And("User click save comparison")
    public void userClickSaveComparison() {
        companyPage.clickSaveComparison();
        companyPage.clickSaveComparisonAs();
    }

    @And("User input unique comparison name")
    public void userInputUniqueComparisonName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        comparisonName = "Comparison " + randomNum;
        companyPage.inputNewComparisonName(comparisonName);
    }

    @And("User confirm save comparison")
    public void userConfirmSaveComparison() {
        companyPage.clickConfirmSaveComparison();
    }

    @Then("Comparison table is succesfully saved")
    public void comparisonTableIsSuccesfullySaved() {
        Assert.assertEquals("Comparison table saved", companyPage.getSuccessAlertMessage());
    }

    @And("New comparison table is displayed")
    public void newComparisonTableIsDisplayed() {
        companyPage.clickSaveComparison();
        companyPage.scrollToLatestComparison();
        Assert.assertEquals(comparisonName, companyPage.getLatestSavedComparisonName());
    }

    @When("User click delete latest comparison table")
    public void userClickDeleteLatestComparisonTable() {
        companyPage.clickDeleteLatestComparison();
    }

    @Then("New comparison table is not displayed")
    public void newComparisonTableIsNotDisplayed() {
        Assert.assertNotEquals(comparisonName, companyPage.getLatestSavedComparisonName());
    }

    @And("User navigate to company Consensus section")
    public void userNavigateToCompanyConsensusSection() {
        companyPage.clickNavTab(COMPANY_TAB_CONSENSUS);
    }

    @Then("User is at company Consensus section")
    public void userIsAtCompanyConsensusSection() {
        String title = "Analyst Financial Estimate";
        Assert.assertTrue(companyPage.isOnConsensusPage(title));
    }

    @When("User hover on one of the consensus graph")
    public void userHoverOnOneOfTheConsensusGraph() {
        companyPage.hoverConsensusGraph();
    }

    @Then("Consensus graph is shown")
    public void consensusGraphIsShown() {
        Assert.assertTrue(companyPage.isTooltipGraphDisplayed());
    }

    @And("User navigate to company Corporate Action section")
    public void userNavigateToCompanyCorporateActionSection() {
        companyPage.clickNavTab(COMPANY_TAB_CORP_ACTION);
    }

    @And("User click corporate action tab {string}")
    public void userClickCorporateActionTab(String tab) {
        companyPage.clickCorpActionTab(tab);
    }

    @Then("Corporate action tab {string} is active")
    public void corporateActionTabIsActive(String tab) {
        Assert.assertTrue(companyPage.isCorpActionTabActive(tab));
    }

    @And("User navigate to company Insider section")
    public void userNavigateToCompanyInsiderSection() {
        companyPage.clickNavTab(COMPANY_TAB_INSIDER);
    }

    @And("User click investor profile")
    public void userClickInvestorProfile() {
        companyPage.clickInvestorProfile();
    }

    @Then("User is redirected to investor profile with {string} stock present")
    public void userIsRedirectedToInvestorProfileWithStockPresent(String symbol) {
        Assert.assertTrue(companyPage.isInsiderExist(symbol));
    }

    @And("User click navigate to Corporate Action")
    public void userClickCorporateAction(){
        companyPage.clickNavigate();

        companyPage.clickCorporateAction();
    }
}
