package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.*;
import io.cucumber.java.en.And;
import org.junit.Assert;

public class AmendOrderSteps {

    AmendOrderPage amendOrderPage = new AmendOrderPage();

    @And("User is on portfolio page")
    public void userIsOnPortofolioPage(){
        Assert.assertTrue(amendOrderPage.isOnPortofolioPage());
    }

    @And("User click order tab")
    public void userClickOrder(){
        amendOrderPage.clickOrder();
    }

    @And("User click Amend")
    public void userClickAmend() {
        amendOrderPage.clickAmend();
    }

    @And("User get order status {string}")
    public void userGetOrderStatus(String status){
        //Assert.assertEquals(status, amendOrderPage.userGetOderStatus());
    }

    @And("User click buy in company profile")
    public void userClickBuy(){
        amendOrderPage.clickBuy();
    }

    @And("User click add order")
    public void userClickAddOrder(){
        amendOrderPage.clickAddOrder();
    }

    @And("User set order price to less")
    public void userClickLessOrder(){
        amendOrderPage.clickLessOrderPrice();
    }

}
