package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.CalendarPage;
import id.web.gosoft.automation.pages.stockbit.CompanyPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class CalendarSteps {
    MainPage mainPage = new MainPage();
    CalendarPage calendarPage = new CalendarPage();
    CompanyPage companyPage = new CompanyPage();
    String symbol;

    @And("User click calendar menu")
    public void userClickCalendar() {
        mainPage.clickCalendarMenu();
    }

    @And("User click calendar submenu {string}")
    public void userClickCalendarSubmenu(String submenu) {
        calendarPage.clickSubmenu(submenu);
    }

    @Then("Economic table is displayed")
    public void economicTableIsDisplayed() {
        Assert.assertTrue(calendarPage.isEconomicTableDisplayed());
    }

    @And("User click a symbol in the calendar table")
    public void userClickASymbolInTheCalendarTable() {
        symbol = calendarPage.getFirstSymbol();
        calendarPage.clickSymbol();
    }

    @Then("User is redirected to company page according to calendar")
    public void userIsRedirectedToCompanyPageAccordingToCalendar() {
        Assert.assertEquals(symbol, companyPage.getCompanySymbol());
    }

    @Then("IPO table is displayed")
    public void ipoTableIsDisplayed() {
        Assert.assertTrue(calendarPage.isIpoTableDisplayed());
    }
}
