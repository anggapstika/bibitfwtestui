package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.StreamPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class EarlyBirdSteps {

    StreamPage streamPage = new StreamPage();

    @And("User click button ayo daftar in portrait ad")
    public void userClickAyoDaftarAd(){
        streamPage.clickAyoDaftar();
    }

    @Then("Page generate to stockbit sekuritas registration")
    public void userAtSekuritasRegistrationPage(){
        Assert.assertTrue(streamPage.userAtSekuritasRegistrationPage());
    }

    @And("User click button ayo daftar in landscape ad")
    public void userClickAyoDaftarInLanscapeAd() {
        streamPage.clickLanscapeAd();
    }


}
