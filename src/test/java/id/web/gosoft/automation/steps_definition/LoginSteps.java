package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.LandingPage;
import id.web.gosoft.automation.pages.stockbit.LoginPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class LoginSteps {
    LandingPage landingPage = new LandingPage();
    LoginPage loginPage = new LoginPage();
    MainPage mainPage = new MainPage();

    @Given("User is on landing page")
    public void userIsOnLandingPage() {
        Assert.assertTrue(landingPage.isOnPage());
    }

    @When("User click menu login")
    public void userClickMenuLogin() {
        landingPage.clickMenuLogin();
    }

    @And("User input username as {string}")
    public void userInputUsernameAs(String username) {
        loginPage.inputUsername(username);
    }

    @And("User input password as {string}")
    public void userInputPasswordAs(String password) {
        loginPage.inputPassword(password);
    }

    @And("User click button login")
    public void userClickButtonLogin() {
        loginPage.clickBtnLogin();
    }

    @Then("User is on homepage")
    public void userIsOnHomepage() {
        Assert.assertTrue(mainPage.isOnPage());
    }

    @Then("User gets error {string}")
    public void userGetsError(String error) {
        Assert.assertEquals(error, mainPage.getAlertMessage());
    }

    @Then("User gets warning on username field {string}")
    public void userGetsWarningOnUsernameField(String warning) {
        //Assert.assertEquals(warning, loginPage.getUsernameWarning());
    }

    @Then("Button login is still visible")
    public void buttonLoginIsStillVisible() {
        Assert.assertTrue(loginPage.isBtnLoginDisplayed());
    }

    @Then("User gets warning on password field {string}")
    public void userGetsWarningOnPasswordField(String warning) {
        Assert.assertEquals(warning, loginPage.getPasswordWarning());
    }

    @When("User click login with Facebook button")
    public void userClickLoginWithFacebookButton() {
        loginPage.clickLoginWithFacebookButton();
    }

    @When("User changes to other window")
    public void userChangesToOtherWindow() {
        loginPage.switchToNewlyOpenedWindow();
    }

    @When("User input facebook email {string}")
    public void userInputFacebookEmail(String email) {
        loginPage.inputFacebookEmail(email);
    }

    @When("User input facebook password {string}")
    public void userInputFacebookPassword(String password) {
        loginPage.inputFacebookPassword(password);
    }

    @When("User click facebook login button")
    public void userClickFacebookLoginButton() {
        loginPage.clickFacebookLoginButton();
    }

    @When("User changes back to parent window")
    public void userChangesBackToParentWindow() {
        loginPage.switchToParentWindow();
    }

    @When("User click login with Google button")
    public void userClickLoginWithGoogleButton() {
        loginPage.clickLoginWithGoogleButton();
    }

    @When("User input Google email {string}")
    public void userInputGoogleEmail(String email) {
        loginPage.inputGoogleEmail(email);
    }

    @When("User click Google next button")
    public void userClickGoogleNextButton() {
        loginPage.clickGoogleNextButton();
    }

    @When("User input Google password {string}")
    public void userInputGooglePassword(String password) {
        loginPage.inputGooglePassword(password);
    }

    @And("User close profile avatar popup")
    public void userCloseProfile() {
        loginPage.userCloseAvatarPopup();
    }

}

