package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.MainPage;
import id.web.gosoft.automation.pages.stockbit.ValuationPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;

public class ValuationSteps {
    MainPage mainPage = new MainPage();
    ValuationPage valuationPage = new ValuationPage();

    int randomNum;
    String valuationName;
    String valuationNote;

    @And("User click valuations menu")
    public void userClickValuationsMenu() {
        mainPage.clickValuationMenu();
    }

    @And("User select valuation method {string}")
    public void userSelectValuationMethod(String method) {
        valuationPage.selectValuationMethod(method);
    }

    @And("User choose valuation symbol {string}")
    public void userChooseValuationSymbol(String symbol) {
        valuationPage.inputQuerySymbol(symbol);
        valuationPage.clickFirstSearchResult();
    }

    @And("User select valuation growth {string}")
    public void userSelectValuationGrowth(String growth) {
        valuationPage.selectValuationGrowth(growth);
    }

    @And("User select valuation multiple {string}")
    public void userSelectValuationMultiple(String multiple) {
        valuationPage.selectValuationMultiple(multiple);
    }

    @And("User click value button")
    public void userClickValueButton() {
        valuationPage.clickValueBtn();
    }

    @Then("Valuation result is displayed")
    public void valuationResultIsDisplayed() {
        Assert.assertTrue(valuationPage.isValuationResultPresent());
    }

    @When("User input unique valuation note")
    public void userInputUniqueValuationNote() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        valuationNote = "Note " + randomNum;
        valuationPage.inputValuationNote(valuationNote);
    }

    @And("User click save valuation")
    public void userClickSaveValuation() {
        valuationPage.clickSaveValuation();
    }

    @And("User input unique valuation name")
    public void userInputUniqueValuationName() {
        valuationName = "Valuation " + randomNum;
        valuationPage.inputValuationName(valuationName);
    }

    @And("User confirm save valuation")
    public void userConfirmSaveValuation() {
        valuationPage.clickConfirmCreate();
    }

    @Then("Saved valuation name is displayed")
    public void savedValuationNameIsDisplayed() {
        Assert.assertEquals(valuationName, valuationPage.getValuationName());
    }

    @And("Saved valuation note is displayed")
    public void savedValuationNoteIsDisplayed() {
        Assert.assertEquals(valuationNote, valuationPage.getValuationNote());
    }

    @Then("User gets warning on symbol field {string}")
    public void userGetsWarningOnSymbolField(String warn) {
        Assert.assertEquals(warn, valuationPage.getSymbolWarning());
    }

    @When("User delete valuation")
    public void userDeleteValuation() {
        valuationPage.clickValuationMenuBtn();
        valuationPage.clickDeleteValuationFromMenu();
        valuationPage.clickConfirmDelete();
    }

    @And("Valuation is successfully deleted")
    public void valuationIsSuccessfullyDeleted() {
        String alert = "Template " + valuationName + " has been deleted";
        Assert.assertEquals(alert, valuationPage.getSuccessAlertMessage());
        valuationPage.openListValuationMenu();
        Assert.assertFalse(valuationPage.isValuationExist(valuationName));
    }

    @And("User click update valuation")
    public void userClickUpdateValuation() {
        valuationPage.clickConfirmUpdate();
    }

    @When("User edit valuation name")
    public void userEditValuationName() {
        valuationPage.clickValuationMenuBtn();
        valuationPage.clickEditValuationFromMenu();
        valuationName = "Valuation " + randomNum;
        valuationPage.inputEditValuationName(valuationName);
        valuationPage.clickConfirmUpdate();
    }

    @Then("Valuation is successfully updated")
    public void valuationIsSuccessfullyUpdated() {
        String alert = "Your template has been updated";
        Assert.assertEquals(alert, valuationPage.getSuccessAlertMessage());
    }
}
