package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.MainPage;
import id.web.gosoft.automation.pages.stockbit.ScreenerPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ScreenerSteps {
    MainPage mainPage = new MainPage();
    ScreenerPage screenerPage = new ScreenerPage();

    int randomNum;
    String screenerName;

    @And("User click screener menu")
    public void userClickScreenerMenu() {
        mainPage.clickScreenerMenu();
    }

    @And("User click preset screener button")
    public void userClickPresetScreenerButton() {
        screenerPage.clickPresetScreenerBtn();
    }

    @And("User choose {string} preset filter")
    public void userChoosePresetFilter(String query) {
        screenerPage.inputPresetScreener(query);
        screenerPage.chooseFirstScreenerSearchResult();
    }

    @Then("{string} preset is chosen")
    public void presetIsChosen(String name) {
        Assert.assertEquals(name, screenerPage.getScreenerName());
    }

    @And("User click create new screener button")
    public void userClickCreateNewScreenerButton() {
        screenerPage.clickCreateScreenerBtn();
    }

    @And("User input unique screener name")
    public void userInputUniqueScreenerName() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        screenerName = "Screener " + randomNum;
        screenerPage.inputScreenerName(screenerName);
    }

    @And("User choose {string} as stock universe")
    public void userChooseAsStockUniverse(String stock) {
        screenerPage.openSelectStockUniverse();
        screenerPage.selectStockUniverse(stock);
    }

    @And("User remove default rule")
    public void userRemoveDefaultRule() {
        screenerPage.removeDefaultRule();
    }

    @And("User click Tambah Rules")
    public void userClickTambahRules() {
        screenerPage.clickAddRuleBtn();
    }

    @And("User choose Basic Ratio")
    public void userChooseBasicRatio() {
        screenerPage.clickAddBasicRatio();
    }

    @And("User set {string} as first financial metric")
    public void userSetAsFirstFinancialMetric(String metric) {
        screenerPage.inputFirstFinancialMetric(metric);
    }

    @And("User choose {string} operator")
    public void userChooseOperator(String op) {
        screenerPage.selectOperator(op);
    }

    @And("User input angka pembanding basic as {string}")
    public void userInputAngkaPembandingBasicAs(String value) {
        screenerPage.inputBasicValue(value);
    }

    @And("User choose Ratio vs Ratio")
    public void userChooseRatioVsRatio() {
        screenerPage.clickAddRatioVsRatio();
    }

    @And("User input angka pembanding ratio as {string}")
    public void userInputAngkaPembandingRatioAs(String value) {
        screenerPage.inputRatioValue(value);
    }

    @And("User set {string} as second financial metric")
    public void userSetAsSecondFinancialMetric(String metric) {
        screenerPage.inputSecondFinancialMetric(metric);
    }

    @And("User click save screener")
    public void userClickSaveScreener() {
        screenerPage.clickSaveScreener();
    }

    @And("User select newest saved screener")
    public void userSelectNewestSavedScreener() {
        screenerPage.openFavouriteScreenerMenu();
        screenerPage.selectNewestScreener();
    }

    @Then("Screener name is displayed")
    public void screenerNameIsDisplayed() {
        Assert.assertEquals(screenerName, screenerPage.getScreenerName());
    }

    @And("Metrics {string}, {string}, and {string} are displayed")
    public void metricsAndAreDisplayed(String metric1, String metric2, String metric3) {
        List<String> metrics = screenerPage.getScreenerMetrics();

        Assert.assertEquals(4, metrics.size());

        Assert.assertEquals(metric1, metrics.get(1));
        Assert.assertEquals(metric2, metrics.get(2));
        Assert.assertEquals(metric3, metrics.get(3));
    }

    @Then("Saved screener is present")
    public void savedScreenerIsPresent() {
        screenerPage.openFavouriteScreenerMenu();
        screenerPage.waitABit(3);
        Assert.assertEquals(screenerName, screenerPage.getLatestSavedScreenerName());
    }

    @When("User delete newly saved screener")
    public void userDeleteNewlySavedScreener() {
        screenerPage.deleteLatestScreener();
    }

    @Then("Saved screener doesn't exist anymore")
    public void savedScreenerDoesnTExistAnymore() {
        screenerPage.openFavouriteScreenerMenu();
        Assert.assertNotEquals(screenerName, screenerPage.getLatestSavedScreenerName());
    }

    @And("User remove {string} screener if favorited")
    public void userRemoveScreenerIfFavorited(String name) {
        screenerPage.openFavouriteScreenerMenu();
        screenerPage.removeFavouritedScreener(name);
    }

    @And("User choose level {int} preset {string}")
    public void userChooseLevelPreset(int level, String name) {
        screenerPage.choosePresetAtLevel(level, name);
    }

    @And("User favorited {string} preset screener")
    public void userFavoritedPresetScreener(String name) {
        screenerPage.favoritedPresetScreener(name);
    }

    @And("User close preset screener menu")
    public void userClosePresetScreenerMenu() {
        screenerPage.closePresetScreenerMenu();
    }

    @Then("{string} preset is favorited")
    public void presetIsFavorited(String name) {
        screenerPage.openFavouriteScreenerMenu();
        Assert.assertTrue(screenerPage.isScreenerFavorited(name));
    }

    @And("User favourite {string} screener from {string}, {string} if not yet")
    public void userFavouriteScreenerFromIfNotYet(String level3, String level1, String level2) {
        screenerPage.openFavouriteScreenerMenu();
        if (!screenerPage.isScreenerFavorited(level3)) {
            screenerPage.clickPresetScreenerBtn();
            screenerPage.choosePresetAtLevel(1, level1);
            screenerPage.choosePresetAtLevel(2, level2);
            screenerPage.favoritedPresetScreener(level3);
            screenerPage.closePresetScreenerMenu();
        }
    }

    @When("User remove {string} preset from favourites")
    public void userRemovePresetFromFavourites(String name) {
        screenerPage.removeFavouritedScreener(name);
    }

    @Then("{string} preset is not favorited")
    public void presetIsNotFavorited(String name) {
        Assert.assertFalse(screenerPage.isScreenerFavorited(name));
    }

    @And("Metric {string} is displayed")
    public void metricIsDisplayed(String metric) {
        List<String> metrics = screenerPage.getScreenerMetrics();

        Assert.assertEquals(1, metrics.size());
        Assert.assertEquals(metric, metrics.get(0));
    }

    @When("User click edit screener")
    public void userClickEditScreener() {
        screenerPage.clickEditScreener();
    }

    @And("User remove latest rule")
    public void userRemoveLatestRule() {
        screenerPage.removeLatestRule();
    }

    @And("User click add financial column")
    public void userClickAddFinancialColumn() {
        screenerPage.clickAddFinancialColumnBtn();
    }

    @And("User add {string} to be displayed")
    public void userAddToBeDisplayed(String query) {
        screenerPage.inputAddFinancialColumn(query);
        screenerPage.chooseFirstScreenerSearchResult();
    }
}
