package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.FinancialsPage;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static id.web.gosoft.automation.constants.AutomationConstants.*;

public class FinancialsSteps {
    MainPage mainPage = new MainPage();
    FinancialsPage financialsPage = new FinancialsPage();
    String currentWord;

    @And("User click financials menu")
    public void userClickFinancialsMenu() {
        mainPage.clickFinancialsMenu();
    }

    @And("User select report type as {string}")
    public void userSelectReportTypeAs(String report) {
        financialsPage.selectReportType(report);
    }

    @And("User select statement type as {string}")
    public void userSelectStatementType(String statement) {
        financialsPage.selectStatementType(statement);
    }

    @And("User select currency type as {string}")
    public void userSelectCurrencyType(String currency) {
        financialsPage.selectCurrency(currency);
    }

    @And("User select order type as {string}")
    public void userSelectOrderType(String ordertype) {
        financialsPage.selectOrdertype(ordertype);
    }

    @Then("Financial table title is {string}")
    public void financialTableTitleIs(String title) {
        Assert.assertEquals(title, financialsPage.getTableTitle());
    }

    @And("User sort financials table by {string} order")
    public void userSortFinancialsTableByOrder(String order) {
        financialsPage.clickSortTable(order);
    }

    @Then("Financials table is sorted by {string} order")
    public void financialsTableIsSortedByOrder(String order) {
        switch (order) {
            case SORT_ASCENDING:
                Assert.assertTrue(financialsPage.isSortedAscending());
                break;
            case SORT_DESCENDING:
                Assert.assertTrue(financialsPage.isSortedDescending());
                break;
        }
    }

    @And("User change financials table display to {string}")
    public void userChangeFinancialsTableDisplayTo(String unit) {
        financialsPage.clickChangeUnit(unit);
    }

    @Then("Financials table is displayed in {string}")
    public void financialsTableIsDisplayedIn(String unit) {
        switch (unit) {
            case FINANCIAL_DISPLAY_RAW:
                Assert.assertFalse(financialsPage.isValueHavePercent());
                break;
            case FINANCIAL_DISPLAY_PERCENT:
                Assert.assertTrue(financialsPage.isValueHavePercent());
                break;
        }
    }

    @And("User click round down")
    public void userClickRoundDown() {
        financialsPage.clickRoundDown();
    }

    @Then("Value is rounded to millions")
    public void valueIsRoundedToMillions() {
        Assert.assertEquals(financialsPage.getRoundedToMillions(), financialsPage.getTableTextIdr());
    }

    @Then("Value is rounded to thousands")
    public void valueIsRoundedToThousands() {
        Assert.assertEquals(financialsPage.getRoundedToThousands(), financialsPage.getTableTextIdr());
    }

    @When("User click round up")
    public void userClickRoundUp() {
        financialsPage.clickRoundUp();
    }

    @Then("Value is rounded to billions")
    public void valueIsRoundedToBillions() {
        Assert.assertEquals(financialsPage.getRoundedToBillions(), financialsPage.getTableTextIdr());
    }
}
