package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.base.BasePageObject;
import id.web.gosoft.automation.pages.stockbit.MainPage;
import io.cucumber.java.en.And;

public class BaseSteps {
    BasePageObject basePageObject = new BasePageObject();
    MainPage mainPage = new MainPage();

    @And("User wait for {int} seconds")
    public void userWaitForSeconds(int seconds) {
        basePageObject.waitABit(seconds);
    }

    @And("User scroll to top of page")
    public void userScrollToTopOfPage() {
        basePageObject.scrollToTop();
    }

    @And("User scroll to bottom of page")
    public void userScrollToBottomOfPage() {
        basePageObject.scrollToBottom();
    }

    @And("User switch to newest browser tab")
    public void userSwitchToNewestBrowserTab() {
        mainPage.toLatestTab();
    }

    @And("User logout")
    public void userLogout() {
        mainPage.logout();
    }

    @And("User click header menu")
    public void userClickHeaderMenu() {
        mainPage.clickHeaderMenu();
    }
}
