package id.web.gosoft.automation.steps_definition;

import id.web.gosoft.automation.pages.stockbit.MainPage;
import id.web.gosoft.automation.pages.stockbit.RightMenuPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.concurrent.ThreadLocalRandom;


public class RightMenuSteps {
    RightMenuPage rightMenuPage = new RightMenuPage();
    MainPage mainPage = new MainPage();

    int randomNum;
    String valuationName;
    String valuationNote;

    @And("User click price alert right menu")
    public void userClickPriceAlertRightMenu(){
        rightMenuPage.clickPriceAlertRightMenu();
    }

    @And("User click New Alert")
    public void userClickNewAlert(){
        rightMenuPage.clickNewAlert();
    }

    @And("User input stocksname as {string}")
    public void userInputStocksName(String stck){
        rightMenuPage.inputStocksName(stck);
    }

    @And("User choose combobox condition as {string}")
    public void userChooseComboboxCondition(String condition){
        rightMenuPage.selectCondition(condition);
    }

    @And("User input on value as {string}")
    public void userInputOnValueAs(String val){
        rightMenuPage.inputValue(val);
    }

    @And("User input on desc as {string}")
    public void userInputOnDescAs(String desc){
        rightMenuPage.inputDesc(desc);
    }

    @And("User click button create")
    public void userClickButtonCreate(){
        rightMenuPage.clickButtonCreate();
    }

    @Then("User get display price alert is available")
    public void userGetDisplayProceAlert(){
            rightMenuPage.displayedPriceAlert();
    }

    @And("User delete price alert")
    public void userDeletePriceAlert(){
        rightMenuPage.clickDeleteAlert();
    }

    @And("User click delete button Yes")
    public void userClickDeleteButtonYes(){
        rightMenuPage.clickDeleteYesButton();
    }

    @Then("User succesfully deleting price alert")
    public void userSuccesfullyDeletingPriceAlert(){

    }

    @And("User click on Hotlist right menu")
    public void userClickOnHotlistRightMenu(){
        rightMenuPage.clickRightMenuHotlist();
    }

    @Then("User get section {string}")
    public void userGetSection(String sct){
        Assert.assertEquals(sct,rightMenuPage.getHotlistSection());
    }

    @And("User click active symbol")
    public void userClickActiveSymbol(){
        rightMenuPage.clickRightMenuHotlist();
    }

    @Then("User going to product detail")
    public void userGoingToProdDetail(){

    }

    @And("User click on Valuation right menu")
    public void userClickOnValuationRightMenu(){
        rightMenuPage.clickValuatioanRightMenu();
    }

    @And("User scrolling section kenneth fisher panel")
    public void userScrollingSectionKennethFisher(){
        rightMenuPage.srollingRightPanel();
    }

    @And("User select valuation method right panel {string}")
    public void userSelectValuationMethodRightMenu(String method){
        rightMenuPage.selectValuationMethod(method);
    }

    @And("User choose valuation symbol right panel {string}")
    public void userChooseValuationSymbolRoghtMenu(String symbol){
        rightMenuPage.inputQuerySymbol(symbol);
        rightMenuPage.clickFirstSearchResult();
    }

    @And("User select valuation growth right panel {string}")
    public void userSelectValuationGrowthRightMenu(String growth){
        rightMenuPage.selectValuationGrowth(growth);
    }

    @And("User select valuation multiple right panel {string}")
    public void userSelectValuationMultipleRightMenu(String multiple){
        rightMenuPage.selectValuationMultiple(multiple);
    }

    @And("User click value button on right panel")
    public void userClickValueButtonOnRightMenu(){
        rightMenuPage.clickValueButtonRightMenu();
    }

    @Then("Valuation result is displayed on right panel")
    public void valuationResultIsDisplayed() {
        Assert.assertTrue(rightMenuPage.isValuationResultPresent());
    }

    @When("User input unique valuation note on right panel")
    public void userInputUniqueValuationNote() {
        randomNum = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
        valuationNote = "Note " + randomNum;
        rightMenuPage.inputValuationNote(valuationNote);
    }

    @And("User click save valuation on right panel")
    public void userClickSaveValuation() {
        rightMenuPage.clickSaveValuation();
    }

    @And("User input unique valuation name on right panel")
    public void userInputUniqueValuationName() {
        valuationName = "Valuation " + randomNum;
        rightMenuPage.inputValuationName(valuationName);
    }

    @And("User confirm save valuation on right panel")
    public void userConfirmSaveValuation() {
        rightMenuPage.clickConfirmCreate();
    }

    @Then("Saved valuation name is displayed on right panel")
    public void savedValuationNameIsDisplayed() {
        Assert.assertEquals(valuationName, rightMenuPage.getValuationName());
    }

    @And("Saved valuation note is displayed on right panel")
    public void savedValuationNoteIsDisplayed() {
        Assert.assertEquals(valuationNote, rightMenuPage.getValuationNote());
    }

    @Then("User gets warning on symbol field {string} on right panel")
    public void userGetsWarningOnSymbolField(String warn) {
        Assert.assertEquals(warn, rightMenuPage.getSymbolWarning());
    }

    @When("User delete valuation on right panel")
    public void userDeleteValuation() {
        rightMenuPage.clickValuationMenuBtn();
        rightMenuPage.clickDeleteValuationFromMenu();
        rightMenuPage.clickConfirmDelete();
    }

    @And("Valuation is successfully deleted on right panel")
    public void valuationIsSuccessfullyDeleted() {
        String alert = "Template " + valuationName + " has been deleted";
        Assert.assertEquals(alert, rightMenuPage.getSuccessAlertMessage());
        rightMenuPage.openListValuationMenu();
        Assert.assertFalse(rightMenuPage.isValuationExist(valuationName));
    }

    @And("User click update valuation on right panel")
    public void userClickUpdateValuation() {
        rightMenuPage.clickConfirmUpdate();
    }

    @When("User edit valuation name on right panel")
    public void userEditValuationName() {
        rightMenuPage.clickValuationMenuBtn();
        rightMenuPage.clickEditValuationFromMenu();
        valuationName = "Valuation " + randomNum;
        rightMenuPage.inputEditValuationName(valuationName);
        rightMenuPage.clickConfirmUpdate();
    }

    @Then("Valuation is successfully updated on right panel")
    public void valuationIsSuccessfullyUpdated() {
        String alert = "Your template has been updated";
        Assert.assertEquals(alert, rightMenuPage.getSuccessAlertMessage());
    }

    @And("User click on Bandar Detector right menu")
    public void userClickOnBandarDetectorRightMenu(){
        rightMenuPage.clickBandarDetector();
    }

    @And("User input company as {string}")
    public void userInputCompanyAs(String cmpbnd){
        rightMenuPage.inputCompanyBandDetect(cmpbnd);
        rightMenuPage.clickFirstSearch();
    }

    @Then("Succesfully saved")
    public void succesfullySaved(){

    }

}
