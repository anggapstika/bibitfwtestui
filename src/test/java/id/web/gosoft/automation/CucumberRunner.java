package id.web.gosoft.automation;


import io.cucumber.java.Before;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.platform.engine.Cucumber;
import io.cucumber.spring.CucumberContextConfiguration;

import org.springframework.boot.test.context.SpringBootTest;


@CucumberContextConfiguration
@Cucumber
@SpringBootTest
@CucumberOptions(
        plugin = "message:target/cucumber-report.ndjson" ,
        features = {"src/test/resources/id/web/gosoft/automation/features/"} , stepNotifications = true)
public class CucumberRunner {
}
