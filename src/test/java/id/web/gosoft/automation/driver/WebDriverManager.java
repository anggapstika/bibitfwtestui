package id.web.gosoft.automation.driver;

import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebDriverManager implements DriverSource {
    @Override
    public WebDriver newDriver() {
        io.github.bonigarcia.wdm.WebDriverManager.chromedriver().setup();
        io.github.bonigarcia.wdm.WebDriverManager.chromedriver().setup();
        ChromeOptions option=new ChromeOptions();
        option.setPageLoadStrategy(PageLoadStrategy.NONE);
        WebDriver webDriver = new ChromeDriver(option);
        return webDriver;
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
