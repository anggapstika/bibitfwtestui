package id.web.gosoft.automation.hooks;

import id.web.gosoft.automation.webdriver.WebdriverInstance;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.SessionId;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.management.ManagementFactory;

//class WebDriverHooks


public class WebdriverHooks {

    WebdriverInstance webdriverInstance;

    @Before(value = "@Web")
    public void initializeWebdriver() {
        webdriverInstance = new WebdriverInstance();
        webdriverInstance.initialize();
    }

//    @AfterStep(value = "@Web")
//    public void testWeb(Scenario scenario) {
//        System.out.println();
//        SessionId session = ((ChromeDriver) webdriverInstance.webdriverList.get(ManagementFactory.getRuntimeMXBean().getName())).getSessionId();
//        System.out.println("Ini Sessionnya " + session.toString() + " Ini Scenarionya " + scenario.getName());
//    }

    @After(value = "@Web")
    public void quitWebdriver(Scenario scenario) {
        if (scenario.isFailed()) {
            scenario
                    .attach(((TakesScreenshot) webdriverInstance.getDriver()).getScreenshotAs(OutputType.BYTES),
                            "image/png" , "Screenshoot");
            scenario.log("Scenario Fail");
        }
        webdriverInstance.quit();
    }

}
