package id.web.gosoft.automation.hooks;

import id.web.gosoft.automation.webdriver.AndroidDriverInstance;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

//class AndroidDriverHooks

public class AndroidDriverHooks {

  @Before(value = "@Android")
  public void initializeWebdriver() {

    AndroidDriverInstance.initializeWeb();



  }

  @After(value = "@Android")
  public void quitWebdriver(Scenario scenario) {
    if (scenario.isFailed()) {
      scenario
          .attach(((TakesScreenshot) AndroidDriverInstance.webDriver)
                  .getScreenshotAs(OutputType.BYTES),
              "image/png" , "Screenshoot");
      scenario.log("Scenario Fail");
    }
    AndroidDriverInstance.quit();
  }

}
